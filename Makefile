
OBJECTS = $(CPP_SOURCES:.cpp = .o)

CC = g++
INCLUDES = -Iinclude/ -Iinclude/ftd2xx/ -I.

SRC_DIR = ./src

CPP_SOURCES += $(SRC_DIR)/LESO_dev.cpp 
CPP_SOURCES += $(SRC_DIR)/loadD2xx.cpp 
CPP_SOURCES += $(SRC_DIR)/FTDI_error.cpp
CPP_SOURCES += $(SRC_DIR)/leso4Obj.cpp
CPP_SOURCES += $(SRC_DIR)/leso4_export.cpp

ifeq ($(OS),Windows_NT)
	OUTPUT = libLESO4.dll
else
	OUTPUT = libLESO4.so
endif

LIB = -ldl
CFLAGS = -Wall
OUTPUT_TEST = test 

all: $(OUTPUT) 
$(OUTPUT) : $(OBJECTS)
ifeq ($(OS), Windows_NT)
	$(CC) -shared -o $(OUTPUT) $(OBJECTS) $(INCLUDES) $(CFLAGS) -DBUILDING_LESO_LIB_DLL -std=c++0x 
else
	$(CC) -shared -o $(OUTPUT) $(OBJECTS) $(INCLUDES) $(CFLAGS) -std=c++0x -fPIC
endif
clean:
	rm -rfv $(CPP_SOURCES:.cpp=.o) $(OUTPUT)
