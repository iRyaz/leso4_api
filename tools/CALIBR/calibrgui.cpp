
/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\author Shauerman Alexander <shamrel@yandex.ru> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "calibrgui.h"
#include "ui_calibrgui.h"

CalibrGui::CalibrGui(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CalibrGui)
{
    leso4Open("LESO4.1_ER");

    if(!leso4IsOpen())
    {
        QMessageBox::critical(this, tr("Error"), tr("Device not open !!!"));
        std::exit(-1);
    }

    leso4SetSamplesNum(8192);
    leso4SetSamplFreq(sampe_frequency_100kHz);
    leso4EnableChannel(CHANNEL_A);
    leso4EnableChannel(CHANNEL_B);
    leso4EnableChannel(CHANNEL_C);
    leso4EnableChannel(CHANNEL_D);

    ui->setupUi(this);

    connect(&receiveTimer, SIGNAL(timeout()), this, SLOT(interruptTimer()));
    connect(ui->ReadEEPROM, SIGNAL(clicked()), this, SLOT(readEEPROM()));
    connect(ui->WriteEEPROM, SIGNAL(clicked()), this, SLOT(writeEEPROM()));

    calibrA = new ChannelCalibrForm(CHANNEL_A);
    calibrB = new ChannelCalibrForm(CHANNEL_B);
    calibrC = new ChannelCalibrForm(CHANNEL_C);
    calibrD = new ChannelCalibrForm(CHANNEL_D);
    ui->Channels->addTab(calibrA, tr("Channel A"));
    ui->Channels->addTab(calibrB, tr("Channel B"));
    ui->Channels->addTab(calibrC, tr("Channel C"));
    ui->Channels->addTab(calibrD, tr("Channel D"));
    receiveTimer.start(20);
    readEEPROM();
}

CalibrGui::~CalibrGui()
{
    delete calibrA;
    delete calibrB;
    delete calibrC;
    delete calibrD;

    delete ui;
}

void CalibrGui::interruptTimer()
{
    if(leso4ReadFIFO() == -1)
    {
        QMessageBox::critical(this, tr("Error"), tr("Device not connect !!!"));
        std::exit(-1);
    }
}

void CalibrGui::readEEPROM()
{
    leso4BeginCalibration();
    calibrA->updateWidget();
    calibrB->updateWidget();
    calibrC->updateWidget();
    calibrD->updateWidget();
}

void CalibrGui::writeEEPROM()
{
    calibrA->updateCalibr();
    calibrB->updateCalibr();
    calibrC->updateCalibr();
    calibrD->updateCalibr();
    leso4EndCalibration();
}
