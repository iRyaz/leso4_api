
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CALIBR
TEMPLATE = app

RC_FILE = "icons/calibr.rc"

SOURCES += main.cpp\
        calibrgui.cpp \
    channelcalibrform.cpp \
    calibrlineedit.cpp

HEADERS  += calibrgui.h \
    channelcalibrform.h \
    calibr_conf.h \
    calibrlineedit.h

FORMS    += calibrgui.ui \
    channelcalibrform.ui

RESOURCES += \
    resource.qrc

TRANSLATIONS = translations/ru.ts

DISTFILES += translations/*

win32:INCLUDEPATH += ../../../leso4_api
win32:INCLUDEPATH += ../../../leso4_api/include

win32:LIBS += ./libLESO4.dll

linux:LIBS += ../../libLESO4.so
#linux:LIBS += -lLESO4
