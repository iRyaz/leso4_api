
/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\author Shauerman Alexander <shamrel@yandex.ru> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef CHANNELCALIBRFORM_H
#define CHANNELCALIBRFORM_H

#include <QWidget>
#include <QLineEdit>
#include <QDebug>
#include "calibr_conf.h"
#include "calibrlineedit.h"
#include <leso4.h>
#include <leso4calibr.h>

namespace Ui {
class ChannelCalibrForm;
}

class ChannelCalibrForm : public QWidget
{
    Q_OBJECT

    CalibrLineEdit *lineEdit5VZero;
    CalibrLineEdit *lineEdit2VZero;
    CalibrLineEdit *lineEdit1VZero;
    CalibrLineEdit *lineEdit0_5VZero;
    CalibrLineEdit *lineEdit0_2VZero;
    CalibrLineEdit *lineEdit0_1VZero;
    CalibrLineEdit *lineEdit0_05VZero;
    CalibrLineEdit *lineEdit0_02VZero;
    CalibrLineEdit *lineEdit0_01VZero;
    CalibrLineEdit *lineEdit5VAMP;
    CalibrLineEdit *lineEdit2VAMP;
    CalibrLineEdit *lineEdit1VAMP;
    CalibrLineEdit *lineEdit0_5VAMP;
    CalibrLineEdit *lineEdit0_2VAMP;
    CalibrLineEdit *lineEdit0_1VAMP;
    CalibrLineEdit *lineEdit0_05VAMP;
    CalibrLineEdit *lineEdit0_02VAMP;
    CalibrLineEdit *lineEdit0_01VAMP;

    int channel;

public:
    explicit ChannelCalibrForm(int channel_num, QWidget *parent = 0);
    ~ChannelCalibrForm();
    void showAmp(QLineEdit *edit, double amp);
    void showZero(QLineEdit *edit, char zero);

public slots:

    void zeroButton_5V_Click();
    void zeroButton_2V_Click();
    void zeroButton_1V_Click();
    void zeroButton_0_5V_Click();
    void zeroButton_0_2V_Click();
    void zeroButton_0_1V_Click();
    void zeroButton_0_05V_Click();
    void zeroButton_0_02V_Click();
    void zeroButton_0_01V_Click();

    void ampButton_5V_Click();
    void ampButton_2V_Click();
    void ampButton_1V_Click();
    void ampButton_0_5_Click();
    void ampButton_0_2_Click();
    void ampButton_0_1_Click();
    void ampButton_0_05_Click();
    void ampButton_0_02_Click();
    void ampButton_0_01_Click();

    void updateWidget();
    void updateLineEdit(int div);
    void updateCalibr();

private:
    Ui::ChannelCalibrForm *ui;
};

#endif // CHANNELCALIBRFORM_H
