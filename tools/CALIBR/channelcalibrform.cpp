
/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\author Shauerman Alexander <shamrel@yandex.ru> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "channelcalibrform.h"
#include "ui_channelcalibrform.h"

ChannelCalibrForm::ChannelCalibrForm(int channel_num, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChannelCalibrForm)
{

    channel = channel_num;

    ui->setupUi(this);

    connect(ui->zeroButton5V, SIGNAL(clicked()), this, SLOT(zeroButton_5V_Click()));
    connect(ui->zeroButton2V, SIGNAL(clicked()), this, SLOT(zeroButton_2V_Click()));
    connect(ui->zeroButton1V, SIGNAL(clicked()), this, SLOT(zeroButton_1V_Click()));
    connect(ui->zeroButton0_5V, SIGNAL(clicked()), this, SLOT(zeroButton_0_5V_Click()));
    connect(ui->zeroButton0_2V, SIGNAL(clicked()), this, SLOT(zeroButton_0_2V_Click()));
    connect(ui->zeroButton0_1V, SIGNAL(clicked()), this, SLOT(zeroButton_0_1V_Click()));
    connect(ui->zeroButton0_05V, SIGNAL(clicked()), this, SLOT(zeroButton_0_05V_Click()));
    connect(ui->zeroButton0_01V, SIGNAL(clicked()), this, SLOT(zeroButton_0_01V_Click()));
    connect(ui->zeroButton0_02V, SIGNAL(clicked()), this, SLOT(zeroButton_0_02V_Click()));

    connect(ui->ampButton5V, SIGNAL(clicked()), this, SLOT(ampButton_5V_Click()));
    connect(ui->ampButton2V, SIGNAL(clicked()), this, SLOT(ampButton_2V_Click()));
    connect(ui->ampButton1V, SIGNAL(clicked()), this, SLOT(ampButton_1V_Click()));
    connect(ui->ampButton0_5V, SIGNAL(clicked()), this, SLOT(ampButton_0_5_Click()));
    connect(ui->ampButton0_2V, SIGNAL(clicked()), this, SLOT(ampButton_0_2_Click()));
    connect(ui->ampButton0_1V, SIGNAL(clicked()), this, SLOT(ampButton_0_1_Click()));
    connect(ui->ampButton0_05V, SIGNAL(clicked()), this, SLOT(ampButton_0_05_Click()));
    connect(ui->ampButton0_02V, SIGNAL(clicked()), this, SLOT(ampButton_0_02_Click()));
    connect(ui->ampButton0_01V, SIGNAL(clicked()), this, SLOT(ampButton_0_01_Click()));

    lineEdit5VZero = new CalibrLineEdit(ui->lineEdit5VZero);
    lineEdit2VZero = new CalibrLineEdit(ui->lineEdit2VZero);
    lineEdit1VZero = new CalibrLineEdit(ui->lineEdit1VZero);
    lineEdit0_5VZero = new CalibrLineEdit(ui->lineEdit0_5VZero);
    lineEdit0_2VZero = new CalibrLineEdit(ui->lineEdit0_2VZero);
    lineEdit0_1VZero = new CalibrLineEdit(ui->lineEdit0_1VZero);
    lineEdit0_05VZero = new CalibrLineEdit(ui->lineEdit0_05VZero);
    lineEdit0_02VZero = new CalibrLineEdit(ui->lineEdit0_02VZero);
    lineEdit0_01VZero = new CalibrLineEdit(ui->lineEdit0_01VZero);

    connect(lineEdit5VZero, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit2VZero, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit1VZero, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_5VZero, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_2VZero, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_1VZero, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_05VZero, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_02VZero, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_01VZero, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));

    lineEdit5VAMP = new CalibrLineEdit(ui->lineEdit5VAMP);
    lineEdit2VAMP = new CalibrLineEdit(ui->lineEdit2VAMP);
    lineEdit1VAMP = new CalibrLineEdit(ui->lineEdit1VAMP);
    lineEdit0_5VAMP = new CalibrLineEdit(ui->lineEdit0_5VAMP);
    lineEdit0_2VAMP = new CalibrLineEdit(ui->lineEdit0_2VAMP);
    lineEdit0_1VAMP = new CalibrLineEdit(ui->lineEdit0_1VAMP);
    lineEdit0_05VAMP = new CalibrLineEdit(ui->lineEdit0_05VAMP);
    lineEdit0_02VAMP = new CalibrLineEdit(ui->lineEdit0_02VAMP);
    lineEdit0_01VAMP = new CalibrLineEdit(ui->lineEdit0_01VAMP);

    connect(lineEdit5VAMP, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit2VAMP, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit1VAMP, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_5VAMP, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_2VAMP, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_1VAMP, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_05VAMP, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_02VAMP, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
    connect(lineEdit0_01VAMP, SIGNAL(textEdited(QString)), this, SLOT(updateCalibr()));
}

ChannelCalibrForm::~ChannelCalibrForm()
{
    delete ui;
}

void ChannelCalibrForm::zeroButton_5V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_20V);
    leso4ZeroCalibration(channel, max_voltage_20V);
    lineEdit5VZero->setText(QString::number(leso4GetCalibrationZero(channel, max_voltage_20V)));
    ui->ampButton5V->setEnabled(true);
}

void ChannelCalibrForm::zeroButton_2V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_8V);
    leso4ZeroCalibration(channel, max_voltage_8V);
    lineEdit2VZero->setText(QString::number(leso4GetCalibrationZero(channel, max_voltage_8V)));
    ui->ampButton2V->setEnabled(true);
}

void ChannelCalibrForm::zeroButton_1V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_4V);
    leso4ZeroCalibration(channel, max_voltage_4V);
    lineEdit1VZero->setText(QString::number(leso4GetCalibrationZero(channel, max_voltage_4V)));
    ui->ampButton1V->setEnabled(true);
}

void ChannelCalibrForm::zeroButton_0_5V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_2V);
    leso4ZeroCalibration(channel, max_voltage_2V);
    lineEdit0_5VZero->setText(QString::number(leso4GetCalibrationZero(channel, max_voltage_2V)));
    ui->ampButton0_5V->setEnabled(true);
}

void ChannelCalibrForm::zeroButton_0_2V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_800mV);
    leso4ZeroCalibration(channel, max_voltage_800mV);
    lineEdit0_2VZero->setText(QString::number(leso4GetCalibrationZero(channel, max_voltage_800mV)));
    ui->ampButton0_2V->setEnabled(true);
}

void ChannelCalibrForm::zeroButton_0_1V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_400mV);
    leso4ZeroCalibration(channel, max_voltage_400mV);
    lineEdit0_1VZero->setText(QString::number(leso4GetCalibrationZero(channel, max_voltage_400mV)));
    ui->ampButton0_1V->setEnabled(true);
}

void ChannelCalibrForm::zeroButton_0_05V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_200mV);
    leso4ZeroCalibration(channel, max_voltage_200mV);
    lineEdit0_05VZero->setText(QString::number(leso4GetCalibrationZero(channel, max_voltage_200mV)));
    ui->ampButton0_05V->setEnabled(true);
}

void ChannelCalibrForm::zeroButton_0_02V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_80mV);
    leso4ZeroCalibration(channel, max_voltage_80mV);
    lineEdit0_02VZero->setText(QString::number(leso4GetCalibrationZero(channel, max_voltage_80mV)));
    ui->ampButton0_02V->setEnabled(true);
}

void ChannelCalibrForm::zeroButton_0_01V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_40mV);
    leso4ZeroCalibration(channel, max_voltage_40mV);
    lineEdit0_01VZero->setText(QString::number(leso4GetCalibrationZero(channel, max_voltage_40mV)));
    ui->ampButton0_01V->setEnabled(true);
}

void ChannelCalibrForm::showAmp(QLineEdit *edit, double amp)
{
    if(amp != 0)
        amp += AMP_CONV_K; // !!! AMP
    edit->setText(QString::number(amp));
}

void ChannelCalibrForm::showZero(QLineEdit *edit, char zero)
{
    edit->setText(QString::number(zero));
}

void ChannelCalibrForm::ampButton_5V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_20V);
    leso4AmpCalibration(channel, max_voltage_20V);
    showAmp(lineEdit5VAMP, leso4GetCalibrationAmp(channel, max_voltage_20V));
}

void ChannelCalibrForm::ampButton_2V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_8V);
    leso4AmpCalibration(channel, max_voltage_8V);
    showAmp(lineEdit2VAMP, leso4GetCalibrationAmp(channel, max_voltage_8V));
}

void ChannelCalibrForm::ampButton_1V_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_4V);
    leso4AmpCalibration(channel, max_voltage_4V);
    showAmp(lineEdit1VAMP, leso4GetCalibrationAmp(channel, max_voltage_4V));
}

void ChannelCalibrForm::ampButton_0_5_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_2V);
    leso4AmpCalibration(channel, max_voltage_2V);
    showAmp(lineEdit0_5VAMP, leso4GetCalibrationAmp(channel, max_voltage_2V));
}

void ChannelCalibrForm::ampButton_0_2_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_800mV);
    leso4AmpCalibration(channel, max_voltage_800mV);
    showAmp(lineEdit0_2VAMP, leso4GetCalibrationAmp(channel, max_voltage_800mV));
}

void ChannelCalibrForm::ampButton_0_1_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_400mV);
    leso4AmpCalibration(channel, max_voltage_400mV);
    showAmp(lineEdit0_1VAMP, leso4GetCalibrationAmp(channel, max_voltage_400mV));
}

void ChannelCalibrForm::ampButton_0_05_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_200mV);
    leso4AmpCalibration(channel, max_voltage_200mV);
    showAmp(lineEdit0_05VAMP, leso4GetCalibrationAmp(channel, max_voltage_200mV));
}

void ChannelCalibrForm::ampButton_0_02_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_80mV);
    leso4AmpCalibration(channel, max_voltage_80mV);
    showAmp(lineEdit0_02VAMP, leso4GetCalibrationAmp(channel, max_voltage_80mV));
}

void ChannelCalibrForm::ampButton_0_01_Click()
{
    leso4SetAmplitudeScan(channel, max_voltage_40mV);
    leso4AmpCalibration(channel, max_voltage_40mV);
    showAmp(lineEdit0_01VAMP, leso4GetCalibrationAmp(channel, max_voltage_40mV));
}

void ChannelCalibrForm::updateWidget()
{
    for(int i = 0; i < DIV_NUM; i++)
        updateLineEdit(i);
}

void ChannelCalibrForm::updateLineEdit(int _div)
{
    QLineEdit *zeroEdit = lineEdit5VZero;
    QLineEdit *ampEdit = lineEdit5VAMP;
    max_voltage div = max_voltage_20V;

    switch(_div)
    {
    case DIV_5V:
        div = max_voltage_20V;
        zeroEdit = lineEdit5VZero;
        ampEdit = lineEdit5VAMP;
        break;
    case DIV_2V:
        div = max_voltage_8V;
        zeroEdit = lineEdit2VZero;
        ampEdit = lineEdit2VAMP;
        break;
    case DIV_1V:
        div = max_voltage_4V;
        zeroEdit = lineEdit1VZero;
        ampEdit = lineEdit1VAMP;
        break;
    case DIV_05V:
        div = max_voltage_2V;
        zeroEdit = lineEdit0_5VZero;
        ampEdit = lineEdit0_5VAMP;
        break;
    case DIV_02V:
        div = max_voltage_800mV;
        zeroEdit = lineEdit0_2VZero;
        ampEdit = lineEdit0_2VAMP;
        break;
    case DIV_01V:
        div = max_voltage_400mV;
        zeroEdit = lineEdit0_1VZero;
        ampEdit = lineEdit0_1VAMP;
        break;
    case DIV_005V:
        div = max_voltage_200mV;
        zeroEdit = lineEdit0_05VZero;
        ampEdit = lineEdit0_05VAMP;
        break;
    case DIV_002V:
        div = max_voltage_80mV;
        zeroEdit = lineEdit0_02VZero;
        ampEdit = lineEdit0_02VAMP;
        break;
    case DIV_001V:
        div = max_voltage_40mV;
        zeroEdit = lineEdit0_01VZero;
        ampEdit = lineEdit0_01VAMP;
        break;
    }

    showAmp(ampEdit, leso4GetCalibrationAmp(channel, div));
    showZero(zeroEdit, leso4GetCalibrationZero(channel, div));
}

void ChannelCalibrForm::updateCalibr()
{
    leso4SetCalibrationAmp(channel, max_voltage_20V, lineEdit5VAMP->text().toDouble());
    leso4SetCalibrationZero(channel, max_voltage_20V, lineEdit5VZero->text().toInt());

    leso4SetCalibrationAmp(channel, max_voltage_8V, lineEdit2VAMP->text().toDouble());
    leso4SetCalibrationZero(channel, max_voltage_8V, lineEdit2VZero->text().toInt());

    leso4SetCalibrationAmp(channel, max_voltage_4V, lineEdit1VAMP->text().toDouble());
    leso4SetCalibrationZero(channel, max_voltage_4V, lineEdit1VZero->text().toInt());

    leso4SetCalibrationAmp(channel, max_voltage_2V, lineEdit0_5VAMP->text().toDouble());
    leso4SetCalibrationZero(channel, max_voltage_2V, lineEdit0_5VZero->text().toInt());

    leso4SetCalibrationAmp(channel, max_voltage_800mV, lineEdit0_2VAMP->text().toDouble());
    leso4SetCalibrationZero(channel, max_voltage_800mV, lineEdit0_2VZero->text().toInt());

    leso4SetCalibrationAmp(channel, max_voltage_400mV, lineEdit0_1VAMP->text().toDouble());
    leso4SetCalibrationZero(channel, max_voltage_400mV, lineEdit0_1VZero->text().toInt());

    leso4SetCalibrationAmp(channel, max_voltage_200mV, lineEdit0_05VAMP->text().toDouble());
    leso4SetCalibrationZero(channel, max_voltage_200mV, lineEdit0_05VZero->text().toInt());

    leso4SetCalibrationAmp(channel, max_voltage_80mV, lineEdit0_02VAMP->text().toDouble());
    leso4SetCalibrationZero(channel, max_voltage_80mV, lineEdit0_02VZero->text().toInt());

    leso4SetCalibrationAmp(channel, max_voltage_40mV, lineEdit0_01VAMP->text().toDouble());
    leso4SetCalibrationZero(channel, max_voltage_40mV, lineEdit0_01VZero->text().toInt());
}
