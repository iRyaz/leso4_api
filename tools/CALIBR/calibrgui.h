
/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\author Shauerman Alexander <shamrel@yandex.ru> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef CALIBRGUI_H
#define CALIBRGUI_H

#include <QWidget>
#include <QDebug>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <cstdio>
#include <cstdlib>
#include <QLineEdit>
#include <QMessageBox>

#include <fstream>
#include "calibr_conf.h"
#include "channelcalibrform.h"
#include <leso4.h>
#include <leso4calibr.h>

using namespace std;

namespace Ui {
class CalibrGui;
}

class CalibrGui : public QWidget
{
    Q_OBJECT

    ChannelCalibrForm *calibrA;
    ChannelCalibrForm *calibrB;
    ChannelCalibrForm *calibrC;
    ChannelCalibrForm *calibrD;

    QTimer receiveTimer;

public:
    explicit CalibrGui(QWidget *parent = 0);
    ~CalibrGui();

public slots:
    void interruptTimer();
    void writeEEPROM();
    void readEEPROM();

signals:
    void updateTabWidget();

private:
    Ui::CalibrGui *ui;
};

#endif // CALIBRGUI_H
