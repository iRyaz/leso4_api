
/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\author Shauerman Alexander <shamrel@yandex.ru> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef CALIBRLINEEDIT_H
#define CALIBRLINEEDIT_H

#include <QLineEdit>
#include <QKeyEvent>
#include <QDebug>

class CalibrLineEdit : public QLineEdit
{
    bool keyFilter(int key);
    bool isTextEmpty();
    bool findCharMinus();
    bool findCharInterval();
    bool findChar(char c);
    char getLastChar();
    char getFirstChar();
public:
    CalibrLineEdit(QLineEdit *parent);
    ~CalibrLineEdit();

protected:
    virtual void	keyPressEvent(QKeyEvent * event);
    virtual void focusOutEvent(QFocusEvent * e);
};

#endif // CALIBRLINEEDIT_H
