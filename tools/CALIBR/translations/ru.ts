<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CalibrGui</name>
    <message>
        <location filename="../calibrgui.ui" line="14"/>
        <source>LESO calibr</source>
        <translation type="unfinished">LESO калибровка</translation>
    </message>
    <message>
        <location filename="../calibrgui.ui" line="33"/>
        <source>Write to EEPROM</source>
        <translation type="unfinished">Запись в EEPROM</translation>
    </message>
    <message>
        <location filename="../calibrgui.ui" line="40"/>
        <source>Read EEPROM</source>
        <translation type="unfinished">Чтение EEPROM</translation>
    </message>
    <message>
        <location filename="../calibrgui.cpp" line="30"/>
        <source>Channel A</source>
        <translation type="unfinished">Канал А</translation>
    </message>
    <message>
        <location filename="../calibrgui.cpp" line="31"/>
        <source>Channel B</source>
        <translation type="unfinished">Канал В</translation>
    </message>
    <message>
        <location filename="../calibrgui.cpp" line="32"/>
        <source>Channel C</source>
        <translation type="unfinished">Канал С</translation>
    </message>
    <message>
        <location filename="../calibrgui.cpp" line="33"/>
        <source>Channel D</source>
        <translation type="unfinished">Канал D</translation>
    </message>
    <message>
        <location filename="../calibrgui.cpp" line="149"/>
        <source>Error</source>
        <translation type="unfinished">Ошибка</translation>
    </message>
    <message>
        <location filename="../calibrgui.cpp" line="149"/>
        <source>LESO4.1 not connect</source>
        <translation type="unfinished">Нет связи с прибором</translation>
    </message>
</context>
<context>
    <name>ChannelCalibrForm</name>
    <message>
        <location filename="../channelcalibrform.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="24"/>
        <source>5V/div</source>
        <translation type="unfinished">5 В/Дел</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="41"/>
        <location filename="../channelcalibrform.ui" line="105"/>
        <location filename="../channelcalibrform.ui" line="169"/>
        <location filename="../channelcalibrform.ui" line="237"/>
        <location filename="../channelcalibrform.ui" line="301"/>
        <location filename="../channelcalibrform.ui" line="365"/>
        <location filename="../channelcalibrform.ui" line="433"/>
        <location filename="../channelcalibrform.ui" line="497"/>
        <location filename="../channelcalibrform.ui" line="561"/>
        <source>Zero</source>
        <translation type="unfinished">Ноль</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="48"/>
        <location filename="../channelcalibrform.ui" line="72"/>
        <location filename="../channelcalibrform.ui" line="112"/>
        <location filename="../channelcalibrform.ui" line="136"/>
        <location filename="../channelcalibrform.ui" line="176"/>
        <location filename="../channelcalibrform.ui" line="200"/>
        <location filename="../channelcalibrform.ui" line="244"/>
        <location filename="../channelcalibrform.ui" line="268"/>
        <location filename="../channelcalibrform.ui" line="308"/>
        <location filename="../channelcalibrform.ui" line="332"/>
        <location filename="../channelcalibrform.ui" line="372"/>
        <location filename="../channelcalibrform.ui" line="396"/>
        <location filename="../channelcalibrform.ui" line="440"/>
        <location filename="../channelcalibrform.ui" line="464"/>
        <location filename="../channelcalibrform.ui" line="504"/>
        <location filename="../channelcalibrform.ui" line="528"/>
        <location filename="../channelcalibrform.ui" line="568"/>
        <location filename="../channelcalibrform.ui" line="592"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="65"/>
        <source>5V</source>
        <translation type="unfinished">5 В</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="88"/>
        <source>2V/div</source>
        <translation type="unfinished">2 В/Дел</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="129"/>
        <source>2V</source>
        <translation type="unfinished">2 В</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="152"/>
        <source>1V/div</source>
        <translation type="unfinished">1 В/Дел</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="193"/>
        <source>1V</source>
        <translation type="unfinished">1 В</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="220"/>
        <source>0,5V/div</source>
        <translation type="unfinished">0.5 В/Дел</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="261"/>
        <source>0.5V</source>
        <translation type="unfinished">0.5 В</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="284"/>
        <source>0,2V/div</source>
        <translation type="unfinished">0.2 В/Дел</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="325"/>
        <source>0.2V</source>
        <translation type="unfinished">0.2 В</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="348"/>
        <source>0,1V/div</source>
        <translation type="unfinished">0.1 В/Дел</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="389"/>
        <source>0.1V</source>
        <translation type="unfinished">0.1 В</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="416"/>
        <source>0,05V/div</source>
        <translation type="unfinished">0.05 В/Дел</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="457"/>
        <source>0.05V</source>
        <translation type="unfinished">0.05 В</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="480"/>
        <source>0,02V/div</source>
        <translation type="unfinished">0.02 В/Дел</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="521"/>
        <source>0.02V</source>
        <translation type="unfinished">0.02 В</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="544"/>
        <source>0,01V/div</source>
        <translation type="unfinished">0.01 В/Дел</translation>
    </message>
    <message>
        <location filename="../channelcalibrform.ui" line="585"/>
        <source>0.01V</source>
        <translation type="unfinished">0.01 В</translation>
    </message>
</context>
</TS>
