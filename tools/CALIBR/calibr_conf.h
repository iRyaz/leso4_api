
/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\author Shauerman Alexander <shamrel@yandex.ru> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef CALIBR_CONF_H
#define CALIBR_CONF_H

#define DIV_5V 0
#define DIV_2V 1
#define DIV_1V 2
#define DIV_05V 3
#define DIV_02V 4
#define DIV_01V 5
#define DIV_005V 6
#define DIV_002V 7
#define DIV_001V 8

#define SAMPLE_DUMP_FILE "samples.txt"

#define AMP_CONV_K 0.0f

#endif // CALIBR_CONF_H
