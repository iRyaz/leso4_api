
/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\author Shauerman Alexander <shamrel@yandex.ru> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "calibrlineedit.h"

CalibrLineEdit::CalibrLineEdit(QLineEdit *parent) : QLineEdit(parent)
{
}

CalibrLineEdit::~CalibrLineEdit()
{

}

void	CalibrLineEdit::keyPressEvent(QKeyEvent * event)
{
    if(keyFilter(event->key()))
        QLineEdit::keyPressEvent(event);
}

bool CalibrLineEdit::keyFilter(int key)
{
    switch(key)
    {
    case Qt::Key_Backspace:
    case Qt::Key_0:
    case Qt::Key_1:
    case Qt::Key_2:
    case Qt::Key_3:
    case Qt::Key_4:
    case Qt::Key_5:
    case Qt::Key_6:
    case Qt::Key_7:
    case Qt::Key_8:
    case Qt::Key_9: return true;
    case Qt::Key_Minus: if(findCharMinus())
            return false;
        else return !isTextEmpty();
    case Qt::Key_Period: if(findCharInterval())
            return false;
        else return isTextEmpty();
    }

    return false;
}

bool CalibrLineEdit::isTextEmpty()
{
    if(this->text().isEmpty())
        return false;
    return true;
}

bool CalibrLineEdit::findCharInterval()
{
    return findChar('.');
}

bool CalibrLineEdit::findCharMinus()
{
    return findChar('-');
}

bool CalibrLineEdit::findChar(char c)
{
    int str_len = this->text().toStdString().size();
    const char *s_ptr = this->text().toStdString().data();
    for(int i = 0; i < str_len; i++)
    {
        if(s_ptr[i] == c)
            return true;
    }

    return false;
}

char CalibrLineEdit::getLastChar()
{
    int str_len = this->text().toStdString().size();
    const char *s_ptr = this->text().toStdString().data();
    return s_ptr[str_len-1];
}

char CalibrLineEdit::getFirstChar()
{
    const char *s_ptr = this->text().toStdString().data();
    return s_ptr[0];
}

void CalibrLineEdit::focusOutEvent(QFocusEvent * e)
{
    if(getLastChar() == '.')
    {
        if(getFirstChar() == '.')
            this->setText("0");
        else
            this->setText(this->text() + "0");
    }
    if(getLastChar() == '-')
        this->setText("1");

    QLineEdit::focusOutEvent(e);
}
