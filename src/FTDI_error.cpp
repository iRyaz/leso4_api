/**
\file FTDI_error.cpp
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "FTDI_error.h"

FTDI_error::FTDI_error(const char* func_name, FT_STATUS e_status)
{	
	//message.error_procedure = func_name;
	message.lastStatus = e_status;
	message.type = FTDI_TYPE_ERROR_FT_STATUS_ERROR;
}

FTDI_error::FTDI_error(ErrorMessage& m)
{
	message = m;
}

const char* FTDI_error::get_BugFunctionName()
{
	return message.error_procedure;//.data();
}

FT_STATUS FTDI_error::lastErrorStatus()
{
	return message.lastStatus;
}

ErrorMessage* FTDI_error::getErrorMessage()
{
	return &message;
}


