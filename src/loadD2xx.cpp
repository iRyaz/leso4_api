
/**
\file loadD2xx.cpp
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "loadD2xx.h"

FTDI_LIB::FTDI_LIB()
{
    is_load_flag = false;	
    ftdi_lib_name = NULL;
	d2xx = new D2XXDriver;
}

FTDI_LIB::~FTDI_LIB()
{
    if(is_load_flag)
        unload();	    	
	delete d2xx;
}

void FTDI_LIB::setName(const char *libName)
{
    ftdi_lib_name = libName;	
}

int FTDI_LIB::load()
{
#ifdef __linux__	
    d2xx_lib_handle = dlopen(ftdi_lib_name, RTLD_LAZY);
#elif _unix__
	d2xx_lib_handle = dlopen(ftdi_lib_name, RTLD_LAZY);
#elif _WIN32
	d2xx_lib_handle = LoadLibrary(ftdi_lib_name);
#endif	
	
	if(d2xx_lib_handle == NULL)
        	return -1;
	
    return initFunctionPtr();	
}

#ifdef __linux__ 
void * FTDI_LIB::sym(void *handle, const char *func_name)
#elif __unix__
void * FTDI_LIB::sym(void *handle, const char *func_name)
#elif _WIN32 
FARPROC FTDI_LIB::sym(HINSTANCE handle, const char *func_name)
#endif 
{
#ifdef __linux__
	return dlsym(handle, func_name);
#elif __unix__
	return dlsym(handle, func_name);
#elif _WIN32 
	return GetProcAddress(handle, func_name);
#endif
}

int FTDI_LIB::initFunctionPtr()
{	
    IS_NULL((d2xx->FT_OpenEx_e = (FT_OpenEx_t)sym(d2xx_lib_handle, "FT_OpenEx")))
    IS_NULL(d2xx->FT_Close_e = (FT_Close_t)sym(d2xx_lib_handle, "FT_Close"))
    IS_NULL(d2xx->FT_Read_e = (FT_Read_t)sym(d2xx_lib_handle, "FT_Read"))
    IS_NULL(d2xx->FT_Write_e = (FT_Write_t)sym(d2xx_lib_handle, "FT_Write"))
    IS_NULL(d2xx->FT_ResetDevice_e = (FT_ResetDevice_t)sym(d2xx_lib_handle, "FT_ResetDevice"))
    IS_NULL(d2xx->FT_Purge_e = (FT_Purge_t)sym(d2xx_lib_handle, "FT_Purge"))
    IS_NULL(d2xx->FT_SetBitMode_e = (FT_SetBitMode_t)sym(d2xx_lib_handle, "FT_SetBitMode"))
    IS_NULL(d2xx->FT_GetStatus_e = (FT_GetStatus_t)sym(d2xx_lib_handle, "FT_GetStatus"))
    IS_NULL(d2xx->FT_SetTimeouts_e = (FT_SetTimeouts_t)sym(d2xx_lib_handle, "FT_SetTimeouts"))
    IS_NULL(d2xx->FT_GetQueueStatusEx_e = (FT_GetQueueStatusEx_t)sym(d2xx_lib_handle, "FT_GetQueueStatusEx"))
    IS_NULL(d2xx->FT_GetQueueStatus_e = (FT_GetQueueStatus_t)sym(d2xx_lib_handle, "FT_GetQueueStatus"))
    IS_NULL(d2xx->FT_EE_UASize_e = (FT_EE_UASize_t)sym(d2xx_lib_handle, "FT_EE_UASize"))
    IS_NULL(d2xx->FT_EE_UAWrite_e = (FT_EE_UAWrite_t)sym(d2xx_lib_handle, "FT_EE_UAWrite"))
    IS_NULL(d2xx->FT_EE_UARead_e = (FT_EE_UARead_t)sym(d2xx_lib_handle, "FT_EE_UARead"))
    IS_NULL(d2xx->FT_SetLatencyTimer_e = (FT_SetLatencyTimer_t)sym(d2xx_lib_handle, "FT_SetLatencyTimer"))
    IS_NULL(d2xx->FT_SetUSBParameters_e = (FT_SetUSBParameters_t)sym(d2xx_lib_handle, "FT_SetUSBParameters"))
	IS_NULL(d2xx->FT_GetStatus_e = (FT_GetStatus_t)sym(d2xx_lib_handle, "FT_GetStatus"))
    is_load_flag = true;
    return 0; // Function Init Success    	
}

void FTDI_LIB::unload()
{
    is_load_flag = false;	
#ifdef __linux__	
    dlclose(d2xx_lib_handle);
#elif __unix__
	dlclose(d2xx_lib_handle);
#elif _WIN32
	FreeLibrary(d2xx_lib_handle);
#endif	
}

