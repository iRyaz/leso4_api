
/**
\file LESO_dev.cpp
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "LESO_dev.h"

LESO_dev::LESO_dev(char *rxBuffer, int rxDataSize) 
{
    	isOpenFlag = false;
    	arraySize = rxDataSize;
    	dataArray = rxBuffer;
    	ftdi_lib.setName(D2XX_LIB);
    	ftdi_lib.load();
	
    /*if(ftdi_lib.is_load() == false)
    {
        ErrorMessage sendError = { FTDI_TYPE_ERROR_DYNAMIC_LIB_ERROR,
								NULL, NULL, 0 };
        throw new FTDI_error(sendError);
    }*/
	
	if(ftdi_lib.is_load())	
		lesoFtdi = ftdi_lib.getDriver();
}

LESO_dev::~LESO_dev()
{
    ftdi_lib.unload();
}

LESO_STATUS LESO_dev::open(char *descriptor)
{
	FT_STATUS __status;
	
	if(ftdi_lib.is_load())
		__status = lesoFtdi->FT_OpenEx_e(PVOID(descriptor), FT_OPEN_BY_DESCRIPTION, &leso_handle);
	else
	{
		ErrorMessage sendError = { FTDI_TYPE_ERROR_DYNAMIC_LIB_ERROR,
									NULL, NULL, 0 };
        throw new FTDI_error(sendError);
	}		
	
    switch(__status)
    {
        case FT_OK: readCalibr(); isOpenFlag = true; return LESO_OPEN;
        case FT_DEVICE_NOT_OPENED: return LESO_NOT_OPEN;
        case FT_DEVICE_NOT_FOUND: return LESO_DEVICE_NOT_FOUND;
    }  
	
    isOpenFlag = false;
    return LESO_NOT_OPEN;
}

void LESO_dev::close()
{
	if(isOpenFlag == true)
	{
		FT_STATUS _st = lesoFtdi->FT_Close_e(leso_handle);
		if(_st == FT_OK)
			isOpenFlag = false; // Close Success
		else
			isOpenFlag = true; // Device not close!! 
	}
}

void LESO_dev::reset()
{
    CHECK_FT_STATUS(lesoFtdi->FT_ResetDevice_e(leso_handle))
    settingDeviceLink();
}

void LESO_dev::readCalibr()
{
	int bufSize = getUASize();
	char EEBuf[bufSize];
	readEEPROM(&EEBuf[0], bufSize);
	int bufOffset = 0;
	unsigned char i;
	for (i = 0 ; i<CHANNEL_NUM; i++)
		fillCalibrStruct(&EEBuf[0], &channel_calibr[i][0], CALIBR_NUM, &bufOffset);

	
	//printCalibr(&channel_C_calibr[0], CALIBR_NUM);
}

void LESO_dev::printCalibr(CALIBR_VALUE *cal_ptr, int size)
{
	/*for(int i(0); i < size; i++)
	{
		cout << i+1 << ":  ";
		cout << "amp: " << cal_ptr[i].amp << " ";
		cout << "zero: " << cal_ptr[i].zero << "\n";
		cout << "==========================================\n";
	}*/
}

void LESO_dev::fillCalibrStruct(char *EEMem, CALIBR_VALUE *cal_ptr, int size, int *bufOffset)
{
	
	for(int i(0); i < size; i++)
	{
		cal_ptr[i].zero = (double)EEMem[(*bufOffset)++];
		cal_ptr[i].amp = ((double)(EEMem[*bufOffset]>>8 | EEMem[*bufOffset+1])) / CALIBR_AMP_DIV;
		*bufOffset += 2;
	}
}

void LESO_dev::bitMode()
{
    // Single channel Synchronous 245 FIFO Mode	
    CHECK_FT_STATUS(lesoFtdi->FT_SetBitMode_e(leso_handle, MODE_MASK, FTDI_DEV_MODE))
}

void LESO_dev::timeoutMode()
{
    CHECK_FT_STATUS(lesoFtdi->FT_SetTimeouts_e(leso_handle, READ_FTDI_TIMEOUT, WRITE_FTDI_TIMEOUT))
}

void LESO_dev::latencyTimer()
{
    CHECK_FT_STATUS(lesoFtdi->FT_SetLatencyTimer_e(leso_handle, LATENCY_TIMEOUT))
}

void LESO_dev::settingPurge()
{
    CHECK_FT_STATUS(lesoFtdi->FT_Purge_e(leso_handle, LESO_PURGE_MODE))
}

void LESO_dev::settingDeviceLink()
{
    bitMode();
    timeoutMode();
    latencyTimer();
    settingPurge();  	
}

const char* LESO_dev::getDataPointer()
{
    return dataArray;	
}

int LESO_dev::read()
{
    DWORD rxBytes = 0;
    CHECK_FT_STATUS(lesoFtdi->FT_Read_e(leso_handle, dataArray, arraySize, &rxBytes))
    return rxBytes;   	
}

int LESO_dev::read(int readBytesNum)
{
    DWORD rxBytes = 0;
    CHECK_FT_STATUS(lesoFtdi->FT_Read_e(leso_handle, dataArray, readBytesNum, &rxBytes))
    return rxBytes;   	
}

int LESO_dev::write(unsigned char *bytes, int size)
{
    DWORD writtenBytes = 0;
    CHECK_FT_STATUS(lesoFtdi->FT_Write_e(leso_handle, bytes, size, &writtenBytes))
    return writtenBytes;	
}

PSTATUS LESO_dev::status()
{
    BUFFER_STATUS *status = new BUFFER_STATUS;
    //CHECK_FT_STATUS(lesoFtdi->FT_GetQueueStatus_e(leso_handle, &(status->rxBytes)))
    CHECK_FT_STATUS(lesoFtdi->FT_GetStatus_e(leso_handle, &(status->rxBytes), &(status->txBytes), &(status->event)))
	return status;	
}

int LESO_dev::getReadBytes()
{
	DWORD rx = 0, tx = 0, event = 0;
	CHECK_FT_STATUS(lesoFtdi->FT_GetStatus_e(leso_handle, &rx, &tx, &event))
	return rx;
}

int LESO_dev::queueStatus()
{
	DWORD rx;
	CHECK_FT_STATUS(lesoFtdi->FT_GetQueueStatus_e(leso_handle, &rx))
	return rx;
}

void LESO_dev::purge()
{
	CHECK_FT_STATUS(lesoFtdi->FT_Purge_e(leso_handle, LESO_PURGE_MODE))
}

void LESO_dev::InDeviceSize(int size)
{
    CHECK_FT_STATUS(lesoFtdi->FT_SetUSBParameters_e(leso_handle, size, size))
}

bool LESO_dev::cmd(unsigned char leso_cmd, unsigned char arg)
{
    unsigned char cmdArray[CMD_SIZE] = {leso_cmd, arg};	
    if(write(&cmdArray[0], CMD_SIZE) == CMD_SIZE)
		return true;
    else
        return false;	
}

int LESO_dev::getUASize()
{
    DWORD size = 0;
    CHECK_FT_STATUS(lesoFtdi->FT_EE_UASize_e(leso_handle, &size))
    return size;
}

int LESO_dev::readEEPROM(char *eeBufPtr, int size)
{
    DWORD retSize = 0;
    CHECK_FT_STATUS(lesoFtdi->FT_EE_UARead_e(leso_handle, (unsigned char*)eeBufPtr, (unsigned int)size, &retSize))
    return retSize;
}

void LESO_dev::writeEEPROM(char *eeBufPtr, int buf_len)
{
    CHECK_FT_STATUS(lesoFtdi->FT_EE_UAWrite_e(leso_handle, (unsigned char*)eeBufPtr, (unsigned int)buf_len))
}

