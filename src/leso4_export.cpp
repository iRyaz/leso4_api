
/**
\file leso4_export.cpp
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "leso4Obj.h"
#include "leso4.h"

#include <iostream>
#include <cstring>

LESO4 leso4_obj;
int ErrorCode;

void setErrorCode(FTDI_error *e)
{
	if(e->getErrorMessage()->type == FTDI_TYPE_ERROR_DYNAMIC_LIB_ERROR)
		ErrorCode = ERROR_CODE_FTDI_LIB_NOT_LOADED;
	else
		ErrorCode = ERROR_CODE_DEVICE_NOT_FOUND;
}

LESO4_STATUS leso4SetSamplesNum(int num)
{
	ErrorCode = NO_ERROR_CODE;
	if(num == leso4GetSamplesNum()) return LESO4_OK;

	try
	{
		leso4_obj.samples(num);
	}
	catch(FTDI_error *e)
	{
		return LESO4_ERROR;
		setErrorCode(e);
	}
	
	return LESO4_OK;
}

int leso4GetSamplesNum()
{
	return leso4_obj.getProperty()->samplesNum;
}

LESO4_STATUS leso4Open(const char *descriptor)
{
	char status;
	ErrorCode = NO_ERROR_CODE;
	try
	{
		status = leso4_obj.open((char*)descriptor);
	}
	catch(FTDI_error *e)
	{
		setErrorCode(e);
		return LESO4_ERROR;
	}
	
	if(status != LESO_OPEN)
	{
		if(status == LESO_DEVICE_NOT_FOUND) ErrorCode = ERROR_CODE_DEVICE_NOT_FOUND;
		if(status == LESO_NOT_OPEN)	ErrorCode = ERROR_CODE_DEVICE_NOT_OPEN;
		return LESO4_ERROR;
	}
	return LESO4_OK;
}

int leso4IsOpen()
{
	if(leso4_obj.isOpen())
		return 1;
	else	
		return 0;
}


void leso4CloseDevice()
{
	leso4_obj.close();
}


LESO4_STATUS leso4EnableChannel(int ch)
{
	ErrorCode = NO_ERROR_CODE;
	LESO4_STATUS status = LESO4_OK;
	try
	{
		leso4_obj.enableChannel(ch);
	}
	catch(FTDI_error *e)
	{
		setErrorCode(e);
		status = LESO4_ERROR;
	}
	
	return status;
}

LESO4_STATUS leso4DisableChannel(int ch)
{
	ErrorCode = NO_ERROR_CODE;
	LESO4_STATUS status = LESO4_OK;
	try
	{
		leso4_obj.disableChannel(ch);
	}
	catch(FTDI_error *e)
	{
		setErrorCode(e);
		status = LESO4_ERROR;
	}
	
	return status;
}

LESO4_STATUS leso4SetSamplFreq(sample_frequency freq)
{
	freq = (freq >= sampe_frequency_50MHz) && (freq < sampe_frequency_END) ? freq :  sampe_frequency_2500kHz;

	ErrorCode = NO_ERROR_CODE;
	if(freq == leso4GetSamplFreq()) return LESO4_OK;

	try
	{
		leso4_obj.timeScan(freq);
	}
	catch(FTDI_error *e)
	{
		setErrorCode(e);
		return LESO4_ERROR;
	}
	
	return LESO4_OK;
}

int leso4GetSamplFreq()
{
	return leso4_obj.getProperty()->timeScan;
}

LESO4_STATUS leso4SetAmplitudeScan(int channel, max_voltage ampDiv)
{
	ErrorCode = NO_ERROR_CODE;
	ampDiv = (ampDiv >= max_voltage_20V) && (ampDiv < max_voltage_END) ? ampDiv :  max_voltage_20V;
	LESO4_STATUS status = LESO4_OK;
	// Проверяем, включен ли канал.
	if (!leso4IsChannelEnable(channel)) return  LESO4_OK;
	// Проверяем, может быть, требуемая амплитуда уже установлена.
	if(ampDiv == leso4GetAmplitudeScan(channel)) return  LESO4_OK;

	char arg = 0;
	switch(ampDiv)
	{
		case max_voltage_20V:	arg = ARG_ATTENUATION_1_500;	break;
		case max_voltage_8V:	arg = ARG_ATTENUATION_1_200;	break;
		case max_voltage_4V:	arg = ARG_ATTENUATION_1_100;	break;
		case max_voltage_2V:	arg = ARG_ATTENUATION_1_50;		break;
		case max_voltage_800mV:	arg = ARG_ATTENUATION_1_20;		break;
		case max_voltage_400mV:	arg = ARG_ATTENUATION_1_10;		break;
		case max_voltage_200mV:	arg = ARG_ATTENUATION_1_5;		break;
		case max_voltage_80mV:	arg = ARG_ATTENUATION_1_2;		break;
		case max_voltage_40mV:	arg = ARG_ATTENUATION_1_1;		break;
		default : 				arg = ARG_ATTENUATION_1_500;
	}
	
	try
	{
		leso4_obj.amplitudeScan(channel, (int)arg);
	}
	catch(FTDI_error *e)
	{
		setErrorCode(e);
		status = LESO4_ERROR;
	}
	
	return status;
}

max_voltage leso4GetAmplitudeScan(int channel)
{
	uint8_t ret = 0;
	
	channel = leso4_obj.validChannel(channel);
	ret = leso4_obj.getProperty()->channelSettings[channel].ampScan;

  	// Convert to dial position
	switch(ret)
	{
		case ARG_ATTENUATION_1_500:	return max_voltage_20V;
		case ARG_ATTENUATION_1_200:	return max_voltage_8V;
		case ARG_ATTENUATION_1_100:	return max_voltage_4V;
		case ARG_ATTENUATION_1_50:	return max_voltage_2V;
		case ARG_ATTENUATION_1_20:	return max_voltage_800mV;
		case ARG_ATTENUATION_1_10:	return max_voltage_400mV;
		case ARG_ATTENUATION_1_5:	return max_voltage_200mV;
		case ARG_ATTENUATION_1_2:	return max_voltage_80mV;
		case ARG_ATTENUATION_1_1:	return max_voltage_40mV;
		default: return max_voltage_20V;
	}
	
}

int leso4IsChannelEnable(int channel)
{
	channel = leso4_obj.validChannel(channel);
	return leso4_obj.getProperty()->channelSettings[channel].enable;
}

LESO4_STATUS leso4SetInputMode(int channel, input_mode mode)
{
	channel = leso4_obj.validChannel(channel);
	mode = ((mode == input_mode_open)||(mode == input_mode_close)) ? mode: input_mode_open;

	if(!leso4IsChannelEnable(channel)) return  LESO4_OK;
	if(mode == leso4GetInputMode(channel)) return  LESO4_OK;
	ErrorCode = NO_ERROR_CODE;
	try
	{
		leso4_obj.inputMode(channel, mode);
	}
	catch(FTDI_error *e)
	{
		setErrorCode(e);
		return LESO4_ERROR;
	}
	
	return LESO4_OK;
}

int LESO_DLL leso4GetInputMode(int channel)
{
	channel = leso4_obj.validChannel(channel);
	return leso4_obj.getProperty()->channelSettings[channel].inputMode;
}

LESO4_STATUS leso4Reset()
{
	ErrorCode = NO_ERROR_CODE;
	LESO4_STATUS status = LESO4_OK;
	try
	{
		leso4_obj.resetControl();
	}
	catch(FTDI_error *e)
	{
		setErrorCode(e);
		status = LESO4_ERROR;
	}
	
	return status;
}

LESO4_STATUS leso4ResetFTDI()
{
	ErrorCode = NO_ERROR_CODE;
	LESO4_STATUS status = LESO4_OK;
	try
	{
		leso4_obj.resetFTDI();
	}
	catch(FTDI_error *e)
	{
		setErrorCode(e);
		status = LESO4_ERROR;
	}
	
	return status;
}

int leso4GetVersion()
{
	return leso4_obj.version();
}

int leso4ReadFIFO()
{
	ErrorCode = NO_ERROR_CODE;
	int read_bytes = 0;
	try
	{
		read_bytes = leso4_obj.readFIFO();
	}
	catch(FTDI_error *e)
	{
		setErrorCode(e);
	}
	
	return read_bytes;
}

void* leso4GetData(double* buff, int channel)
{
	leso4_obj.validChannel(channel);
	if(!leso4IsChannelEnable(channel)) return  NULL;

	if(buff == NULL) return (void*)leso4_obj.getFIFO(channel);
	return memcpy(buff, leso4_obj.getFIFO(channel), leso4_obj.getFIFOSize()*sizeof(double));
}


int leso4GetChannelFIFOSize()
{
	return leso4_obj.getFIFOSize();
}

int leso4GetLastErrorCode()
{
	return ErrorCode;
}

void leso4BeginCalibration()
{
	leso4_obj.BeginCalibration();
}

void leso4EndCalibration()
{
	leso4_obj.EndCalibration();
}

double leso4AmpCalibration(int channel, max_voltage div)
{
	return leso4_obj.calculateAmp(channel, div);
}

char leso4ZeroCalibration(int channel, max_voltage div)
{
	return leso4_obj.calculateZero(channel, div);
}

double leso4GetCalibrationAmp(int channel, max_voltage _div)
{
	int div = 0;
	switch(_div)
	{
	case max_voltage_20V: div = 0; break;
	case max_voltage_8V: div = 1; break;
	case max_voltage_4V: div = 2; break;
	case max_voltage_2V: div = 3; break;
	case max_voltage_800mV: div = 4; break;
	case max_voltage_400mV: div = 5; break;
	case max_voltage_200mV: div = 6; break;
	case max_voltage_80mV: div = 7; break;
	case max_voltage_40mV: div = 8; break;
	case max_voltage_END: div = 0; break;
	}
	
	return leso4_obj.getCalibrStruct(channel)[div].amp;
}

char leso4GetCalibrationZero(int channel, max_voltage _div)
{
	int div = 0;
	switch(_div)
	{
	case max_voltage_20V: div = 0; break;
	case max_voltage_8V: div = 1; break;
	case max_voltage_4V: div = 2; break;
	case max_voltage_2V: div = 3; break;
	case max_voltage_800mV: div = 4; break;
	case max_voltage_400mV: div = 5; break;
	case max_voltage_200mV: div = 6; break;
	case max_voltage_80mV: div = 7; break;
	case max_voltage_40mV: div = 8; break;
	case max_voltage_END: div = 0; break;
	}
	
	return leso4_obj.getCalibrStruct(channel)[div].zero;
}

void leso4SetCalibrationAmp(int channel, max_voltage _div, double amp)
{
	int div = 0;
	switch(_div)
	{
	case max_voltage_20V: div = 0; break;
	case max_voltage_8V: div = 1; break;
	case max_voltage_4V: div = 2; break;
	case max_voltage_2V: div = 3; break;
	case max_voltage_800mV: div = 4; break;
	case max_voltage_400mV: div = 5; break;
	case max_voltage_200mV: div = 6; break;
	case max_voltage_80mV: div = 7; break;
	case max_voltage_40mV: div = 8; break;
	case max_voltage_END: div = 0; break;
	}
	
	leso4_obj.getCalibrStruct(channel)[div].amp = amp;
}

void leso4SetCalibrationZero(int channel, max_voltage _div, char zero)
{
	int div = 0;
	switch(_div)
	{
	case max_voltage_20V: div = 0; break;
	case max_voltage_8V: div = 1; break;
	case max_voltage_4V: div = 2; break;
	case max_voltage_2V: div = 3; break;
	case max_voltage_800mV: div = 4; break;
	case max_voltage_400mV: div = 5; break;
	case max_voltage_200mV: div = 6; break;
	case max_voltage_80mV: div = 7; break;
	case max_voltage_40mV: div = 8; break;
	case max_voltage_END: div = 0; break;
	}
	
	leso4_obj.getCalibrStruct(channel)[div].zero = zero;
}
