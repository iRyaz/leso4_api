
/**
\file leso4.cpp 
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/
#include "leso4Obj.h"

LESO4::LESO4()
{
	LESO4_VERSION = 0;
	dev = new LESO_dev(&rxBuffer[0], RX_BUFFER_MAX_SIZE);
}

LESO4::~LESO4()
{
    delete dev;	
}

void LESO4::close()
{
	if(dev->isOpen())
	{
		disableChannel(CHANNEL_A);
		disableChannel(CHANNEL_B);
		disableChannel(CHANNEL_C);
		disableChannel(CHANNEL_D);
		dev->close();
	}
}

LESO_STATUS LESO4::open(char *descriptor)
{
	LESO_STATUS leso4_status;
	
	try
	{
		leso4_status = dev->open(descriptor);
		if(leso4_status != LESO_OPEN)
			return leso4_status;
		descriptor_ptr = descriptor;
		dev->settingDeviceLink();
		LESO4_VERSION = InitVersionNum();
		initDefaultSettings();
		resetFTDI();
	}
	catch(FTDI_error *e)
	{
		throw;
	}
	
	return leso4_status;
}

bool LESO4::isOpen()
{
	return dev->isOpen();
}

int LESO4::InitVersionNum()
{
	if(strcmp(DEVICE_VERSION_7, descriptor_ptr))
		return 7;
	if(strcmp(DEVICE_VERSION_8, descriptor_ptr))
		return 8;
	
	return 7;
}

void LESO4::initDefaultSettings()
{

	// \todo Сделать каналы enume, защитить значение.
	for(uint8_t i = 0; i < CHANNEL_NUM; i++)
	{
		property.channelSettings[i].num = i;
		property.channelSettings[i].inputMode = INPUT_MODE_OPEN;
		property.channelSettings[i].enable = false;
		amplitudeScan(property.channelSettings[i], DEFAULT_AMP_SCAN);
	}
	samples(DEFAULT_SAMPLES);
	timeScan(DEFAULT_TIME_SCAN);
}

LESO_CONTROL *LESO4::getProperty()
{
	return &property;
}

void LESO4::updateSettings()
{
	for(uint8_t i = 0; i < CHANNEL_NUM; i++)
	{
		property.channelSettings[i].num = i;
		amplitudeScan(property.channelSettings[i], property.channelSettings[i].ampScan);
		if(property.channelSettings[i].enable) enableChannel(i);
		else disableChannel(i);
	}
    	samples(property.samplesNum);
    	timeScan(property.timeScan);
}

void LESO4::samples(int _samples_num)
{
	try
	{
		property.samplesNum = _samples_num;
		dev->cmd(SAMPLES_MBYTE_CMD, (_samples_num & 0x000000FF00)>>8);
		dev->cmd(SAMPLES_LBYTE_CMD, _samples_num & 0x00000000FF);
	}
	catch(FTDI_error *e)
	{
		throw;
	}	
}

void LESO4::timeScan(int samp_freq)
{
	property.timeScan = samp_freq;
	//\todo эта хрень должна считаться внутри ПЛИС
	dev->cmd(DECIM_AMP_CMD, (char)(0.5f + log2(samp_freq)*3));
    dev->cmd(DECIM_FACTOR_MSB_CMD, (samp_freq & 0x000000FF00)>>8);
    dev->cmd(DECIM_FACTOR_LSB_CMD, samp_freq & 0x00000000FF);
}

int LESO4::amplitudeCorrection()
{
	int ret = 0;
	switch(property.timeScan)
	{
	case 2000: ret = 24; break;
	case 1000: ret = 24; break;
	case 500: ret = 24; break;
	case 200: ret = 12; break;
	case 100: ret = 12; break;
	case 50: ret = 12; break;
	case 20: ret = 0; break;
	case 10: ret = 0; break;
	case 5: ret = 0; break;
	case 2: ret = -12; break;
	case 1: ret = -12; break;
	}
	
	return ret;
}

void LESO4::amplitudeScan(int ch, int _ampDiv)
{
    	char en = 0;	
    	property.channelSettings[ch].ampScan = _ampDiv;

    	if(property.channelSettings[ch].enable)
        	en = CHANNEL_ON;
    	else 
        	en = CHANNEL_OFF;	    	    
    
    	dev->cmd(getChannelCmd(ch), (property.channelSettings[ch].ampScan )|en);
}

void LESO4::enableChannel(int ch)
{
    	dev->cmd(property.channelSettings[ch], property.channelSettings[ch].ampScan + CHANNEL_ON);
    	property.channelSettings[ch].enable = true;
}

void LESO4::disableChannel(int ch)
{
	amplitudeScan(property.channelSettings[ch], DEFAULT_AMP_SCAN);
	dev->cmd(property.channelSettings[ch], property.channelSettings[ch].ampScan + CHANNEL_OFF);
	property.channelSettings[ch].enable = false;
}

char LESO4::getChannelCmd(int num)
{
	switch(num)
	{
		case CHANNEL_A: return CH_A_CMD; break;
		case CHANNEL_B: return CH_B_CMD; break;
		case CHANNEL_C: return CH_C_CMD; break;
		case CHANNEL_D: return CH_D_CMD; break;
	}

	return CH_A_CMD;
}

void LESO4::inputMode(int ch, uint8_t mode)
{	
	property.channelSettings[ch].inputMode = mode;
	
	uint8_t _input_mode_cmd = 0;

	for(uint8_t i = 0; i < CHANNEL_NUM; i++)
		_input_mode_cmd |= property.channelSettings[i].inputMode << i;
	_input_mode_cmd <<=4;

	dev->cmd(RELAY_CMD, _input_mode_cmd);
}

void LESO4::resetControl()
{
    	initDefaultSettings();	
}

void LESO4::resetFTDI()
{
	dev->reset();
}

void LESO4::resetFIFO()
{
	dev->cmd(RESET_CMD, RESET_CMD);
}

int LESO4::readFIFO()
{
	for(uint8_t i = 0; i < CHANNEL_NUM; i++)
		samplesChannelBuffer[i].clear();

	
	unsigned int readBytes = property.samplesNum*2*4;
	updateSettings();
	
	if(readBytes > RX_BUFFER_MAX_SIZE)
	{
		readBytes = RX_BUFFER_MAX_SIZE;
		samples(readBytes/8);
	}
	
	unsigned int rx = 0;
	
	while(1)
	{
		//dev->purge();
		rx = dev->getReadBytes();
		if(rx == 0)
			break;
		dev->read(rx);
	}

	resetFIFO();
	
	while(1) {
		rx = dev->getReadBytes();
		if(rx >= readBytes)
			break;
	}
	
	int retReadBytes = dev->read(readBytes);
	
	char *channelADataPtr = &rxBuffer[0];
	char *channelBDataPtr = &rxBuffer[1*(retReadBytes/4)];
	char *channelCDataPtr = &rxBuffer[2*(retReadBytes/4)];
	char *channelDDataPtr = &rxBuffer[3*(retReadBytes/4)];
	
	fillChannelVector(CHANNEL_A, channelADataPtr, readBytes/4);
	fillChannelVector(CHANNEL_B, channelBDataPtr, readBytes/4);
	fillChannelVector(CHANNEL_C, channelCDataPtr, retReadBytes/4);
	fillChannelVector(CHANNEL_D, channelDDataPtr, readBytes/4);
	
	return retReadBytes;
}

void LESO4::fillChannelVector(int channel_num, char *rxBufPtr, int rxBufSize)
{
	vector<sample_t> *bufferPtr;
	CALIBR_VALUE *calibrStructPtr;
	MEASURE_CHANNEL *channelStructPtr;
	int calibrStructIndex = 0;
	
	double divIndex = 20;
	channel_num = validChannel(channel_num);

	bufferPtr = &samplesChannelBuffer[channel_num];
	calibrStructPtr = dev->getCalibr(channel_num);
	channelStructPtr = &(property.channelSettings[channel_num]);
	
	switch(channelStructPtr->ampScan)
	{
		case ARG_ATTENUATION_1_500:	calibrStructIndex = 0; divIndex = 20.0f;	break;
		case ARG_ATTENUATION_1_200: calibrStructIndex = 1; divIndex = 8.0f;		break;
		case ARG_ATTENUATION_1_100: calibrStructIndex = 2; divIndex = 4.0f;		break;
		case ARG_ATTENUATION_1_50:	calibrStructIndex = 3; divIndex = 2.0f;		break;
		case ARG_ATTENUATION_1_20:	calibrStructIndex = 4; divIndex = 0.8f;		break;
		case ARG_ATTENUATION_1_10:	calibrStructIndex = 5; divIndex = 0.4f;		break;
		case ARG_ATTENUATION_1_5:	calibrStructIndex = 6; divIndex = 0.2f;		break;
		case ARG_ATTENUATION_1_2:	calibrStructIndex = 7; divIndex = 0.08f;	break;
		case ARG_ATTENUATION_1_1:	calibrStructIndex = 8; divIndex = 0.04f;	break;
	}	
	
	/*
		Формат отсчета в целочисленном виде
		
			         Старший байт                           Младший байт
					 
		| X | X | C | C | M | M | M | M |  ||  | L | L | L | L | L | L | L | L | 
        15                              8      7                               0 
		
		Отсчет в целочисленной форме имеет разрядность всего 12 бит
		
		L - младшие биты отсчета 
		M - старшие биты отсчета 
		С - номер канала из которого пришел отсчет 
		X - неиспользуемые биты 
		
		В алгоритме используются только биты отсчета, остальные биты отбрасываются 
	*/
	
	for(int i(0); i < rxBufSize; i+=2)
	{
		sample_t sample_d;
		samples_10bit sample = (unsigned char)rxBufPtr[i]|(rxBufPtr[i+1]&63)<<8;
		int _channel_num = (sample&12288)>>12;
		if(channel_num == _channel_num)
		{
			sample &= 4095;
			sample_d = (double)sample + amplitudeCorrection();
			sample_d -= 512.0f + calibrStructPtr[calibrStructIndex].zero;
			sample_d /= -512.0f;
			sample_d *= divIndex;
			sample_d /= (-1)*(calibrStructPtr[calibrStructIndex].amp);
			bufferPtr->push_back(sample_d);
		}
	}
}

unsigned int LESO4::middleSample(unsigned char *data_ptr, int range)
{
    int ret = 0;
    for(int i = 0; i < (range*2); i+=2)
    {
        ret += (((int)(data_ptr[i+1]&15)<<8)|data_ptr[i])&1023;
		ret += amplitudeCorrection();
    }

    ret /= range;

    return ret;
}

int LESO4::calculateZero(int channel, max_voltage ampScan)
{
	const char *data_ptr = dev->getDataPointer() + 30;
    unsigned char *samplesPtr = (unsigned char*)&(data_ptr[channel*(65536/CHANNEL_NUM)]);
    int sample = middleSample(&samplesPtr[0]);
    int zero = sample - 512;
	int div = 0;
	switch(ampScan)
	{
	case max_voltage_20V: div = 0; break;
	case max_voltage_8V: div = 1; break;
	case max_voltage_4V: div = 2; break;
	case max_voltage_2V: div = 3; break;
	case max_voltage_800mV: div = 4; break;
	case max_voltage_400mV: div = 5; break;
	case max_voltage_200mV: div = 6; break;
	case max_voltage_80mV: div = 7; break;
	case max_voltage_40mV: div = 8; break;
	case max_voltage_END: div = 0; break;
	}
	
	getCalibrStruct(channel)[div].zero = (char)zero;
	return zero;
}

double LESO4::calculateAmp(int channel, max_voltage ampScan)
{
	const char *data_ptr = dev->getDataPointer() + 30;
    unsigned char *samplesPtr = (unsigned char*)&(data_ptr[channel*(65536/CHANNEL_NUM)]);
    int sample = middleSample(&samplesPtr[0]);
    int zero;
    float d = 0;
    double amp = 0;
    float V = 0;
	int div = 0;
	
	switch(ampScan)
	{
	case max_voltage_20V: div = 0; break;
	case max_voltage_8V: div = 1; break;
	case max_voltage_4V: div = 2; break;
	case max_voltage_2V: div = 3; break;
	case max_voltage_800mV: div = 4; break;
	case max_voltage_400mV: div = 5; break;
	case max_voltage_200mV: div = 6; break;
	case max_voltage_80mV: div = 7; break;
	case max_voltage_40mV: div = 8; break;
	case max_voltage_END: div = 0; break;
	}
	
	zero = getCalibrStruct(channel)[div].zero;
	
    switch(property.channelSettings[channel].ampScan)
    {
    case max_voltage_20V: d = 20.0f; V = 5.0f; break;
    case max_voltage_8V: d = 8.0f; V = 2.0f; break;
    case max_voltage_4V: d = 4.0f; V = 1.0f; break;
    case max_voltage_2V: d = 2.0f; V = 0.5f; break;
    case max_voltage_800mV: d = 0.8f; V = 0.2f; break;
    case max_voltage_400mV: d = 0.4f; V = 0.1f; break;
    case max_voltage_200mV: d = 0.2f; V = 0.05f; break;
    case max_voltage_80mV: d = 0.08f; V = 0.02f; break;
    case max_voltage_40mV: d = 0.04f; V = 0.01f; break;
    default: d = 20.0f; V = 5.0f; break;
    }

    amp = (((sample - (512+zero))/-512.000f)*d)/-V;
    if(AMP_CONV_K < amp)
        amp -= AMP_CONV_K; //!! AMP
    else
        amp = 0;
	
	getCalibrStruct(channel)[div].amp = amp;
    
	return amp;
}

void LESO4::BeginCalibration()
{
	int eeSize = dev->getUASize();
	char *eeBuf = new char[eeSize];
	dev->readEEPROM(eeBuf, eeSize);
		
	for(int ch = 0; ch < CHANNEL_NUM; ch++)
	{
		char *startPtr = &eeBuf[ch*DIV_NUM*3];
		int j = 0;
		for(int i = 0; i < DIV_NUM; i++)
		{
			unsigned int lBit = 0, mBit = 0;
			getCalibrStruct(ch)[i].zero = startPtr[j];
			mBit = startPtr[j+1];
			lBit = startPtr[j+2];
			unsigned int _amp = (mBit<<8)|lBit;
			getCalibrStruct(ch)[i].amp = ((float)_amp) / AMP_PACK;
			j+=3;
		}
	}
}

void LESO4::EndCalibration()
{
	unsigned int len = CHANNEL_NUM*DIV_NUM*3;
	char *eeBuf = new char[len];
	int bufIndex = 0, divIndex = 0;
	for(int ch = 0; ch < CHANNEL_NUM; ch++)
	{
		divIndex = 0;
		while(bufIndex != (DIV_NUM*3*(ch+1)))
		{
			unsigned int _amp = (int)(getCalibrStruct(ch)[divIndex].amp * AMP_PACK);
			char lBit = (char)(_amp&0x000000FF);
			char mBit = (char)(_amp&0x0000FF00);
			eeBuf[bufIndex++] = getCalibrStruct(ch)[divIndex++].zero;
			eeBuf[bufIndex++] = mBit;
			eeBuf[bufIndex++] = lBit;
		}
	}
		
	dev->writeEEPROM(eeBuf, len);
	delete eeBuf;
}

CALIBR_DIV *LESO4::getCalibrStruct(int channel_num)
{
	switch(channel_num)
	{
	case CHANNEL_A: return &CHANNEL_A_CALIBR[0]; break;
	case CHANNEL_B: return &CHANNEL_B_CALIBR[0]; break;
	case CHANNEL_C: return &CHANNEL_C_CALIBR[0]; break;
	case CHANNEL_D: return &CHANNEL_D_CALIBR[0]; break;
	}

	return &CHANNEL_A_CALIBR[0];
}
