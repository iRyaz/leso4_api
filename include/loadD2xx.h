
/**
\file loadD2xx.h
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

/*!
	Класс загрузчик динамической библиотеки FTDI 
	Библиотека FTDI может быть как и стандартная D2xx, так и открытая libFTDI
*/

#ifndef FTDI_LIB_H
#define FTDI_LIB_H

#include <cstdlib>
#include <cstdio>
#include "ftd2xx_export.h"

#define IS_NULL(x) if((x) == NULL) { is_load_flag = false; return -1; }

/*!
	Структура в которой неинициализированные указатели на функции FTDI
*/
typedef struct D2xxFunc_s
{
	FT_OpenEx_t FT_OpenEx_e;  					 						
   	FT_Close_t FT_Close_e;						
	FT_Read_t FT_Read_e;							
   	FT_Write_t FT_Write_e;
   	FT_ResetDevice_t FT_ResetDevice_e;
   	FT_Purge_t FT_Purge_e;
   	FT_SetBitMode_t FT_SetBitMode_e;
   	FT_GetStatus_t FT_GetStatus_e;
   	FT_SetTimeouts_t FT_SetTimeouts_e;
   	FT_GetQueueStatusEx_t FT_GetQueueStatusEx_e;
   	FT_GetQueueStatus_t FT_GetQueueStatus_e;
   	FT_EE_UASize_t FT_EE_UASize_e;
   	FT_EE_UAWrite_t FT_EE_UAWrite_e;
   	FT_EE_UARead_t FT_EE_UARead_e;
   	FT_SetLatencyTimer_t FT_SetLatencyTimer_e;
   	FT_SetUSBParameters_t FT_SetUSBParameters_e;
} D2XXDriver;

class FTDI_LIB
{
    	bool is_load_flag; 					///< Флаг, устанавливается если библиотека загрузилась

#ifdef __linux__ 	
   	void *d2xx_lib_handle; 
#elif __unix__
	void *d2xx_lib_handle;
#elif _WIN32
	HINSTANCE d2xx_lib_handle;
#endif	
    	D2XXDriver *d2xx; 					///< Указатель на структуру - драйвер, через которую есть доступ к функциям FTDI

	int initFunctionPtr(); 					///< Инициализация структуры драйвера

    	const char *ftdi_lib_name;				///< Имя динамической юиюлиотеке FTDI  
	
#ifdef __linux__ 
	void *sym(void *handle, const char *func_name);			
#elif __unix__
	void *sym(void *handle, const char *func_name);
#elif _WIN32 
	FARPROC sym(HINSTANCE handle, const char *func_name);
#endif

public:

    	FTDI_LIB();												
    	~FTDI_LIB();
	
	/*!
		Установить имя загружаемой библиотеки 
		\param[in] Имя динамической библиотеки
	*/
    	void setName(const char *libName);
	
	/*!
		Загрузить библиотеку
		\return 
	*/
    	int load();
	
	/*!
		Выгрузить библиотеку 
	*/
    	void unload();
	
	/*!
		Проверить загружена ли библиотека 
		\return false - если библиотеку не удалось загрузить
	*/
    	bool is_load() { return is_load_flag; };
	
	/*!
		Получить указатель на функции FTDI
		\return указатель на структуру D2XXDriver
	*/
    	D2XXDriver* getDriver() { return d2xx; };
};

#endif
