
/**
\file ftd2xx_export.h
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef FTD2XX_export
#define FTD2XX_export

#ifdef _WIN32
#include <windows.h>
#elif __linux__
#include <dlfcn.h>
#elif __unix__
#include <dlfcn.h>
#endif

//#include <ftd2xx.h>
typedef unsigned long DWORD;
typedef unsigned short WORD;
typedef unsigned long ULONG;
typedef unsigned char UCHAR;
typedef unsigned char BYTE;
typedef void* PVOID;	
//typedef short int DWORD;
typedef unsigned char UCHAR;
typedef void* PVOID;
typedef PVOID LPVOID;
typedef DWORD* LPDWORD;
typedef PVOID FT_HANDLE; 
typedef ULONG FT_STATUS;

typedef UCHAR *PUCHAR;

//
// Device status
//
enum {
	FT_OK,
	FT_INVALID_HANDLE,
	FT_DEVICE_NOT_FOUND,
	FT_DEVICE_NOT_OPENED,
	FT_IO_ERROR,
	FT_INSUFFICIENT_RESOURCES,
	FT_INVALID_PARAMETER,
	FT_INVALID_BAUD_RATE,	//7

	FT_DEVICE_NOT_OPENED_FOR_ERASE,
	FT_DEVICE_NOT_OPENED_FOR_WRITE,
	FT_FAILED_TO_WRITE_DEVICE,
	FT_EEPROM_READ_FAILED,
	FT_EEPROM_WRITE_FAILED,
	FT_EEPROM_ERASE_FAILED,
	FT_EEPROM_NOT_PRESENT,
	FT_EEPROM_NOT_PROGRAMMED,
	FT_INVALID_ARGS,
	FT_NOT_SUPPORTED,
	FT_OTHER_ERROR
};

#define FT_OPEN_BY_SERIAL_NUMBER 1
#define FT_OPEN_BY_DESCRIPTION  2
#define FT_OPEN_BY_LOCATION 4

typedef FT_STATUS  (*FT_OpenEx_t) (
	PVOID pArg1,
    	DWORD Flags,
    	FT_HANDLE *pHandle
);

typedef FT_STATUS (*FT_Close_t) (
    	FT_HANDLE ftHandle
);

typedef FT_STATUS (*FT_Read_t) (
    	FT_HANDLE ftHandle,
    	LPVOID lpBuffer,
    	DWORD nBufferSize,
    	LPDWORD lpBytesReturned
);

typedef FT_STATUS (*FT_Write_t) (
    	FT_HANDLE ftHandle,
    	LPVOID lpBuffer,
    	DWORD nBufferSize,
    	LPDWORD lpBytesWritten
);

typedef FT_STATUS (*FT_ResetDevice_t) (
    	FT_HANDLE ftHandle
);

typedef FT_STATUS (*FT_Purge_t) (
    	FT_HANDLE ftHandle,
    	ULONG Mask
);

typedef FT_STATUS (*FT_SetTimeouts_t) (
    	FT_HANDLE ftHandle,
    	ULONG ReadTimeout,
    	ULONG WriteTimeout
);

typedef FT_STATUS (* FT_GetQueueStatus_t) (
    	FT_HANDLE ftHandle,
    	DWORD *dwRxBytes
);

typedef FT_STATUS (*FT_GetQueueStatusEx_t) (
   	FT_HANDLE ftHandle,
   	DWORD *dwRxBytes
);

typedef FT_STATUS (*FT_GetStatus_t) (
    	FT_HANDLE ftHandle,
    	DWORD *dwRxBytes,
    	DWORD *dwTxBytes,
    	DWORD *dwEventDWord
);

typedef FT_STATUS (*FT_EE_UASize_t) (
    	FT_HANDLE ftHandle,
    	LPDWORD lpdwSize
);

typedef FT_STATUS (*FT_EE_UAWrite_t) (
    	FT_HANDLE ftHandle,
    	PUCHAR pucData,
    	DWORD dwDataLen
);

typedef FT_STATUS (*FT_EE_UARead_t) (
    	FT_HANDLE ftHandle,
    	PUCHAR pucData,
    	DWORD dwDataLen,
    	LPDWORD lpdwBytesRead
);

typedef FT_STATUS (*FT_SetLatencyTimer_t) (
    	FT_HANDLE ftHandle,
    	UCHAR ucLatency
);

typedef FT_STATUS (*FT_SetUSBParameters_t) (
    	FT_HANDLE ftHandle,
    	ULONG ulInTransferSize,
    	ULONG ulOutTransferSize
);

typedef FT_STATUS (*FT_SetBitMode_t) (
    	FT_HANDLE ftHandle,
    	UCHAR ucMask,
    	UCHAR ucEnable
);

#endif // FTD2XX_e

