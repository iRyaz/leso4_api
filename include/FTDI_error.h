
/**
\file FTDI_error.h
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

/*! 
	Класс который представляет контейнер и хранит сообщение и тип ошибки
	Сообщение об ошибки хранится в структуре типа ErrorMessage
	Следует заметить, что использование всех полей ErrorMessage необязательно,
	какие поля записываем, в структуру, такие и читаем когда возникнет Exception
*/

#ifndef FTDI_ERROR_H
#define FTDI_ERROR_H

#include "ftd2xx_export.h"

#define FTDI_TYPE_ERROR_FT_STATUS_ERROR		(-1)	//!< Сбой Функция библиотеки FTDI
#define FTDI_TYPE_ERROR_DYNAMIC_LIB_ERROR	(-2)	//!< Ошибка подключения библиотеки FTDI
#define FTDI_TYPE_ERROR_EXPORT_FUNCTION		(-3)	//!< Ошибка экспорта функции из библиотеки FTDI
#define FTDI_TYPE_ERROR_OPEN_DEVICE			(-4)	//!< Устройство не открыто (Ошибка доступа или прибор не подключен)

struct ErrorMessage
{
	int type;					//!< Тип ошибки
	char* error_string;			//!< Сообщение об ошибки
	char* error_procedure;		//!< Название функции в которой произошла ошибка
	FT_STATUS lastStatus;		//!< Статус функции FTDI
	int flag;					//!< Флаг (опциональный параметер)
};

class FTDI_error
{
 	ErrorMessage message;
    	
public:
	/*!
		\param[in] func_name Имя функции, FTDI где возник сбой
		\param[in] e_status Переменная возвращенная функцией
	*/
    	FTDI_error(const char* func_name, FT_STATUS e_status); 
	
	/*!
		\param[in] структура ErrorMessage
	*/
    	FTDI_error(ErrorMessage& m);
	
	/*!
		Функция возвращает имя функции, где произошел сбой
		
		\return указатель на имя функции, где возникла ошибка
	*/
    	const char* get_BugFunctionName();
	
	/*!
		Получить статус который возвратила функция
		
		\return обычная целочисленное значение
	*/
    	FT_STATUS lastErrorStatus();
	
	/*!
		Получить доступ к переданной структуре
		
		\return указатель на структуру, где информация о сбое
	*/
    	ErrorMessage *getErrorMessage();
};

#endif
