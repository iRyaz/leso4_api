
#ifndef LESO4_CMD_H
#define LESO4_CMD_H


#define GET_VERSION_CMD 0x01

#define RESET_CMD 0xFF
#define DECIM_AMP_CMD 3
#define DECIM_FACTOR_LSB_CMD 61
#define DECIM_FACTOR_MSB_CMD 60
#define RELAY_CMD 6
#define CH_A_CMD 50
#define CH_B_CMD 51
#define CH_C_CMD 52
#define CH_D_CMD 53

#define CHANNEL_ON 3
#define CHANNEL_OFF 0

#define DIV_100_ON 0								// Включить делитель на 100 (первый аттенюатор) во входной цепи 
#define DIV_100_OFF 1								// Выключить делитель на 100 (первый аттенюатор) во входной цепи
#define MUX_ATTENUATION_1_1 3							// Переключить делитель 1:1 (второй аттенюатор)  
#define MUX_ATTENUATION_1_2 0							// Переключить делитель 1:2 (второй аттенюатор)
#define MUX_ATTENUATION_1_5 2							// Переключить делитель 1:5 (второй аттенюатор)
#define MUX_ATTENUATOIN_1_10 1							// Переключить делитель 1:10 (второй аттенюатор)
#define MUX_ATTENUATION_1_20 6							// Переключить делитель 1:20 (второй аттенюатор)
#define MUX_ATTENUATION_1_50 4							// Переключить делитель 1:50 (второй аттенюатор)

#define ARG_ATTENUATION_1_1 (MUX_ATTENUATION_1_1<<3)+(DIV_100_OFF<<2) 	//!< Делитель сигнала 1:1
#define ARG_ATTENUATION_1_2 (MUX_ATTENUATION_1_2<<3)+(DIV_100_OFF<<2)		//!< Делитель сигнала 1:2
#define ARG_ATTENUATION_1_5 (MUX_ATTENUATION_1_5<<3)+(DIV_100_OFF<<2)		//!< Делитель сигнала 1:5
#define ARG_ATTENUATION_1_10 (MUX_ATTENUATOIN_1_10<<3)+(DIV_100_OFF<<2)	//!< Делитель сигнала 1:10
#define ARG_ATTENUATION_1_20 (MUX_ATTENUATION_1_20<<3)+(DIV_100_OFF<<2)	//!< Делитель сигнала 1:20
#define ARG_ATTENUATION_1_50 (MUX_ATTENUATION_1_50<<3)+(DIV_100_OFF<<2)	//!< Делитель сигнала 1:50
#define ARG_ATTENUATION_1_100 (MUX_ATTENUATION_1_1<<3)+(DIV_100_ON<<2)	//!< Делитель сигнала 1:100
#define ARG_ATTENUATION_1_200 (MUX_ATTENUATION_1_2<<3)+(DIV_100_ON<<2)	//!< Делитель сигнала 1:200
#define ARG_ATTENUATION_1_500 (MUX_ATTENUATION_1_5<<3)+(DIV_100_ON<<2)	//!< Делитель сигнала 1:500

// Not Used
#define ARG_ATTENUATION_1_1000 (MUX_ATTENUATOIN_1_10<<3)+(DIV_100_ON<<2)
#define ARG_ATTENUATION_1_2000 (MUX_ATTENUATION_1_20<<3)+(DIV_100_ON<<2)
#define ARG_ATTENUATION_1_5000 (MUX_ATTENUATION_1_50<<3)+(DIV_100_ON<<2)
//

#define SAMPLES_MBYTE_CMD 70
#define SAMPLES_LBYTE_CMD 71

#define LESO_RESET 0xFF

#endif
