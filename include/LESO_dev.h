
/**
\file LESO_dev.h 
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef LESO_DEV_H
#define LESO_DEV_H

#include "FTDI_error.h"
#include "loadD2xx.h"
#include <vector>

#include "leso4.h"

class FTDI_error;

#define CHECK_FT_STATUS(FT_procedure) \
	FT_STATUS __status = FT_procedure ; \
	if(__status != FT_OK) {\
			isOpenFlag = false; \
            lesoFtdi->FT_Close_e(&leso_handle); throw new FTDI_error(#FT_procedure, __status);} 		

#define CHECK_EXPORT_FUNCTION(FUNC_PTR)\
    if(!FUNC_PTR){\
        ErrorMessage sendError = {FTDI_TYPE_ERROR_EXPORT_FUNCTION,\
                              NULL , #FUNC_PTR, NULL };\
        throw new FTDI_error(sendError);}\

#define CE(FUNC_PTR) CHECK_EXPORT_FUNCTION(FUNC_PTR)
						
#define MODE_MASK 0xFF
#define FTDI_DEV_MODE 0x40

#define READ_FTDI_TIMEOUT 10 					 
#define WRITE_FTDI_TIMEOUT 10   				

#define LATENCY_TIMEOUT 2

#define FT_PURGE_RX 1
#define FT_PURGE_TX 2

#define LESO_PURGE_MODE FT_PURGE_RX|FT_PURGE_TX

#define CMD_SIZE 2

#ifdef __linux__
#define D2XX_LIB "/usr/local/lib/libftd2xx.so" 			//!< Имя библиотеки D2xx для Linux
#elif _WIN32
#define D2XX_LIB "ftd2xx.dll"					//!< Имя библиотеки D2xx для Windows
#endif

#define CALIBR_NUM 9 						//!< Число диапазонов измерения по амплитуде

#define CALIBR_AMP_DIV 64.0f 					//!< Делитель считанного с памяти значения

enum LESO_STATUS
{
 	LESO_NOT_OPEN = -1,					///! Устройство не открыто
    	LESO_DEVICE_NOT_FOUND = 0,				///! Устройство не найдено 
    	LESO_OPEN = 1	    					///! Устройство успешно открыто 
};

/*!
	Статус буфера FTDI 
*/
struct BUFFER_STATUS
{
    	DWORD event;      
    	DWORD rxBytes;  					//!< Количество принятых байтов
    	DWORD txBytes;	 					//!< Количество отправленных байтов 
};

/*!
	Калибровочный коэффициент, хранящийся в EEPROM памяти FTDI 
*/
struct CALIBR_VALUE  
{
	double amp; 						//!< Усиление
	double zero; 						//!< Смещение нуля
};

typedef BUFFER_STATUS* PSTATUS;

/*!
	Низкоуровневый класс обертка функций FTDI  
	Предоставляет функцию для отпраки команд устройству,
	также в нем идет чтение и формирование калибровочных коэффициентов 
*/

class LESO_dev
{
	CALIBR_VALUE channel_calibr[CHANNEL_NUM][CALIBR_NUM]; 		///< Массив калибровочных коэффициентов для каналов
	
	/*!
		Инициализация коэффициентов калибровки 
		
		\param[in] EEMem дамп EEPROM памяти 
		\param[in] size число коэффициентов для одного канала (Число диапазонов по амплитуде)
		\param[out] offset позиция в дампе EEPROM памяти 
		\param[out] cal_ptr указатель на массив коэффициентов, которые нужно проинициализировать
	*/
	void fillCalibrStruct(char *EEMem, CALIBR_VALUE *cal_ptr, int size, int *offset);
	
	/*!
		Напечатать в консоли калибровочные коэффициенты 
		\param[in] cal_ptr указатель на массив 
		\param[in] size размер массива 
	*/
	void printCalibr(CALIBR_VALUE *cal_ptr, int size);
	
	bool isOpenFlag;
	
protected:
    	FT_HANDLE leso_handle;
    	FTDI_LIB ftdi_lib;
    	D2XXDriver *lesoFtdi;
    	void bitMode();
    	void timeoutMode();
    	void latencyTimer();
    	void settingPurge();
	void readCalibr();
    	char *dataArray;
    	int arraySize;

public:
	/*!
		\param[in] указатель на rxBuffer буфер приема
		\param[in] размер буфера приема 
	*/
    	LESO_dev(char *rxBuffer, int rxDataSize);
	
    	~LESO_dev();
	
	/*!
		\param[in] descriptor указатель на строку дескриптор устройства
		\return 
	*/
    	LESO_STATUS open(char *descriptor);
	
	/*!
		Проверить открыто ли устройство 
		\return true - если девайс открыт 
	*/
	bool isOpen() { return isOpenFlag; }
	
	/*! 
		Установить параметры соединения микросхемы FTDI 
	*/
    	void settingDeviceLink();
	
	/*!
		Сброс устройства 
	*/
 	void reset();
	
	/*!
		Закрыть устройство 
	*/
    	void close();
	
    	const char *getDataPointer();

	/*!
		Прочитать данные с FTDI
		\return количество прочитанных байтов 	
	*/
    	int read();
	
	/*!
		Прочитать данные с FTDI
		\param[in] readBytesNum количество байтов которые нужно прочитать 
		\return количество прочитанных байтов 	
	*/
	int read(int readBytesNum);
	
	/*!
		\return количество байтов, в буфере приема FTDI 
	*/
	int getReadBytes();
	
	/*!
		\param[in] bytes указатель на массив байтов, которые записываются в FTDI
		\param[in] size размер массива байтов 
		\return фактическое количество записанных байтов 
	*/
    	int write(unsigned char *bytes, int size);
	
	/*!
		Возвратить статус буфера FTDI  
		\warning Не использовать в бесконечном цикле, т.к. может возникнуть переполнение кучи 
		\return указатель на структуру 
	*/
    	PSTATUS status();
	
	/*!
		Возвратить количество принятых байтов в буфере FTDI   
		\return количество принятых байтов 
	*/
	int queueStatus();
	
	/*!
		Очистить буфер FTDI 
	*/
	void purge();
	
	/*!
		Отправить команду на устройство 
		\param[in] leso_cmd команда
		\param[in] arg аргумент команды 
		\return false - если команда не отправилась
	*/
    	bool cmd(unsigned char leso_cmd, unsigned char arg);
    	void InDeviceSize(int size);
	
	/*!
		Размер памяти EEPROM который доступен в FTDI 
		\return размер доступной памяти
	*/
    	int getUASize();
	
	/*!
		Прочитать доступную память EEPROM в выделенный буфер  
		\param[out] eeBufPtr указатель на выделенный буфер, куда записать память EEPROM
		\param[in] size размер памяти 
		\return количество прочитанных байт  	
	*/
    	int readEEPROM(char *eeBufPtr, int size);
	
	/*!
		Записать буфер в память EEPROM FTDI 
		\param[in] eeBufPtr указатель на буфер 
		\param[in] buf_len размер буфера 
	*/
    	void writeEEPROM(char *eeBufPtr, int buf_len);
	
	/*!
		Получить указатель на массив калибровочных коэффициентов канала A 
		\return указатель на массив коэффициентов для канана A
		\todo Проверку на валидность канала.
	*/
	CALIBR_VALUE* getCalibr(int channel) { return &channel_calibr[channel][0]; }
	
	/*! 
		\return число диапазонов по амплитуде 
	*/
	int getCalibrValueNum() { return CALIBR_NUM; }
};

#endif

