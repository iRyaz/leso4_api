
/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef LESO4_H
#define LESO4_H

#include "LESO_dev.h"
#include "leso4_cmd.h"
#include "leso4.h"
#include <cmath>
#include <cstring>
#include <cstdint>

#define DEVICE_VERSION_7 "LESO4.1_ER"
#define DEVICE_VERSION_8 "LESO4.2"
							
#define RX_BUFFER_MAX_SIZE 65536 				//!< Максимальный размер буфера приема в байтах
#define CHANNEL_BUFFER_MAX_SIZE RX_BUFFER_MAX_SIZE/4/2		//!< Максимальное количество отсчетов для одного канала 
 									
#define DEFAULT_SAMPLES SAMPLES_8K				//!< Количество отсчетов по умолчанию
#define DEFAULT_AMP_SCAN ARG_ATTENUATION_1_500 			//!< Развертка по амплитуде по умолчанию  
#define DEFAULT_TIME_SCAN sampe_frequency_2500kHz   					//!< Развертка по времени по умолчанию
 
#define AMP_CONV_K 0.0f
#define AMP_PACK 64 
#define MIDDLE_FACTOR 7000

//typedef unsigned int uint8_t;
typedef int samples_10bit; 					//!< Тип отсчета в целочисленной форма								
typedef double sample_t;					//!< Тип отсчета 

using std::vector;

/*!
	Настройки канала 
*/
struct MEASURE_CHANNEL
{
    int num;							//!< Номер канала 
    uint8_t inputMode;						//!< Тип входа открытый/закрытый
    uint8_t ampScan;						//!< Развертка по амплитуде 
    bool enable;						//!< Состояние канала включен/выключен
    operator int() { return num; }
};

/*!
	Настройки прибора
*/
struct LESO_CONTROL
{
    int timeScan;									//!< Развертка по времени
    int samplesNum; 								//!< Количество отсчетов для одного канала, которые выдают прибор
    MEASURE_CHANNEL channelSettings[CHANNEL_NUM];	//!< Параметры канала A, B, B, D
};

/*!
	Результат калибровки устройства
*/
struct CALIBR_DIV
{
    float amp;	//!< Усиление сигнала	
    char zero;	//!< Ноль сигнала 
};

class LESO4
{
	int LESO4_VERSION;						//!< Номер версии прибора
   	LESO_CONTROL property;					//!< Текущие настройки приборы

   	char rxBuffer[RX_BUFFER_MAX_SIZE];		//!< Буфер приема
	
	char *descriptor_ptr;
     
	void initDefaultSettings();				//!< Установить настройки по умолчанию

	LESO_dev *dev;							//!< Указатель на класс обертку FTDI

	char getChannelCmd(int num);			//!< Получить байт команды управления каналом по номеру канала

	int InitVersionNum();					//!< Определение версии прибора 
	
	void updateSettings(); 					//!< Обновить настройки 
	
	vector<sample_t> samplesChannelBuffer[CHANNEL_NUM];		//!< Буфер отсчетов
	
	/*!
		Преобразовать буфер данных с FTDI в отсчеты для указанного канала 
		Отсчеты имеют тип с плавающей точкой и записываются в буфер отсчетов для заданногог канала 
		При преобразовании используются калибровочные коэффициенты 
		\param[in] channel_num номер канала 
		\param[in] rxBufPtr указатель на буфер данных 
		\param[in] rxBufSize размер буфера данных - он меняется в зависимости от количества запрашиваемых данных  
	*/
	void fillChannelVector(int channel_num, char *rxBufPtr, int rxBufSize); 
	
	/*!
		Коррекция амплитуды. Нужна из-за того, что в децимирующем фильтре недостаточно разрядности
	*/
	int amplitudeCorrection();
	
	CALIBR_DIV CHANNEL_A_CALIBR[DIV_NUM];	//!< Калибровка канала A
    CALIBR_DIV CHANNEL_B_CALIBR[DIV_NUM];	//!< Калибровка канала B
    CALIBR_DIV CHANNEL_C_CALIBR[DIV_NUM];	//!< Калибровка канала C
    CALIBR_DIV CHANNEL_D_CALIBR[DIV_NUM];	//!< Калибровка канала D 
	
public:
	LESO4();
    	~LESO4();

	/*!
		Открыть устройство по дескриптору
		Дескриптор содержится в конфигурационной EEPROM микросхемы FTDI
		Указывать пока "LESO4.1_ER"
		\param[in] descriptor указатель на строку дескриптор 
		\return -1 - если открыть не удалось 0 - если устройство не найдено 1 - если устройство открылось
	*/
	LESO_STATUS open(char *descriptor);
	
	bool isOpen();
	
	/*!
		Установить количество отсчетов возвращаемое прибором для одного канала   
		\param[in] _samples_num кол-во отсчетов 
		\warning максимальное значение  8192, если больше максимума, то устанавливается 8192
	*/
    	void samples(int _samples_num);
	
	/*!
		\param[in] ch включить канал с заданным номером 
		\warning каналы нумеруются с нуля, всего их четыре, при некорректном значении включается канал A
	*/
    	void enableChannel(int ch);
	
	/*!
		\param[in] ch выключить канал с заданным номером 
		\warning каналы нумеруются с нуля, всего их четыре, при некорректном значении выключается канал A
	*/
    	void disableChannel(int ch);
	
	/*!
		Функция устанавливает коэффициент децимации ус-ва, тем самым меняется развертка по времени
		Развертка по времени = 1 / опорная частота / коэффициент децимации 
		Опорная частота в приборе 50 МГц
		\param[in] _time развертка по времени 
	*/
    	void timeScan(int _time);
	
	/*!
		Устанавливается развертка по амплитуде для заданного канала
		param[in] ch номер канала 
		param[in] _ampDiv развертка по амплитуде для заданного канала 
	*/
    	void amplitudeScan(int ch, int _ampDiv);
	
	/*!
		Устанавливается режим входа для заданного канала 
		param[in] номер канала  
		param[in] режим входа 
	*/
    	void inputMode(int ch, uint8_t mode);
	
	/*!
		Сброс настроек, выставляются настроки по умолчанию 
	*/
    	void resetControl();
	
	/*!
		Сброс микросхемы FTDI 
	*/
	void resetFTDI();
	
	/*!
		Сброс внутреннего буфера прибора 
	*/
	void resetFIFO();
	
	/*!
		Получение версии прибора 
	*/
	int version() { return LESO4_VERSION; }
	
	/*!
		Чтение данных с прибора 
		Функция читает данные и сразу преобразует их в отсчеты, и копирует эти 
		отсчеты в буфера отсчетов для каждого канала 
		\return количество прочитанных байтов 
	*/
	int readFIFO();
	
	/*!
		Закрытие устройства
	*/
	void close();
	
	/*!
		Получение указателя к структуре, где хранятся текущие настройки прибра 
	*/
	LESO_CONTROL *getProperty();
	
	/*!
		Получение указателя на буфер отсчетов канала
	*/
	sample_t *getFIFO(int channel) { return samplesChannelBuffer[validChannel(channel)].data(); }
	
	/*!
		Получение размера одного буфера отсчетов каждого канала 
	*/
	int getFIFOSize() { return samplesChannelBuffer[CHANNEL_A].size(); }

	/*!
		Проверяем актуальность значения канала
	*/
	int validChannel(int channel) { return ((channel>=0)&&(channel<CHANNEL_NUM)) ? channel : CHANNEL_A; }
	
	/*!
		Дополнительные функции расчета смещения амплитуды и усиления сигнала
		Применяются для калибровки прибора 
	*/
	int calculateZero(int channel, max_voltage ampScan);
	double calculateAmp(int channel, max_voltage ampScan);
	
	/*!
		Прочитать калибровочные коэффициенты из памяти EEPROM
		Эта функция вызывается перед началом калибровки
	*/
	void BeginCalibration();	
	
	/*!
		Записать калибровочные коэффициенты в память EEPROM
		Функция вызывается по окончанию калибровки 	
	*/
	void EndCalibration();
	
	/*!
		Получить коэффициент калибровки амплитуды
	*/
	double getCalibrationAmp(int channel, max_voltage div);
	
	/*!
		Получить коэффициент калибровки нуля 
	*/
	char getCalibrationZero(int channel, max_voltage div);
	
	CALIBR_DIV *getCalibrStruct(int channel_num);
	
private:
	/*!
		Высчитать среднее значение отсчетов 
		\param[in] data_ptr указатель на отсчеты в сыром виде 
		\param[in] range количество отсчетов для усреднения 
	*/
	unsigned int middleSample(unsigned char *data_ptr, int range = MIDDLE_FACTOR);
	
};

#endif
