
/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\author Shauerman Alexander <shamrel@yandex.ru> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details
\version 0.3 
\date 30.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef LESO4_CALIBR_H
#define LESO4_CALIBR_H

#include <leso4.h>

/*!
	Начать калибровку
	Функция считывает память EEPROM FTDI во внутренний буфер
*/
void LESO_DLL leso4BeginCalibration();

/*!
	Закончить калибровку
	Функция записывает рассчитанные значения в память EEPROM FTDI 
*/
void LESO_DLL leso4EndCalibration();

/*!
	Калибровка амплитуды канала на заданном диапазоне
	\param[in] channel канал который калибруется
	\param[in] div диапазон на котором происходит калибровка
	\return калибровочный коэффициент амплитуды 
	
	Эту функцию следует вызывать после переключением измерения амплитуды на диапазон div
	функцией leso4SetAmplitudeScan(div), и после чтения отсчетов с устройства функцией
	leso4ReadFIFO(). Перед чтением отсчетов, следует также установить количество отсчетов 
	на канал - 8192, leso4SetSamplesNum(8192).
	Пример:
	
		leso4SetAmplitudeScan(max_voltage_20V);	// Установить диапазон 20 В
		leso4SetSamplesNum(8192);	// Установить количество отсчетов с прибора 8192 
		leso4ReadFIFO();			// Прочитать 8192 отсчета с устройства  
		printf("Коэффициент калибровки амплитуды диапазона 20В - %d\n", 
			leso4AmpCalibration(CHANNEL_A, max_voltage_20V));	// Вычислить калибровочный коэффициент 
*/
double LESO_DLL leso4AmpCalibration(int channel, max_voltage div);

/*!
	Калибровка нуля канала на заданном диапазоне
	\param[in] channel канал который калибруется
	\param[in] div диапазон на котором происходит калибровка
	\return калибровочный коэффициент нуля 
	
	Эту функцию следует вызывать после переключением измерения амплитуды на диапазон div
	функцией leso4SetAmplitudeScan(div), и после чтения отсчетов с устройства функцией
	leso4ReadFIFO(). Перед чтением отсчетов, следует также установить количество отсчетов 
	на канал - 8192, leso4SetSamplesNum(8192).
	Пример:
	
		leso4SetAmplitudeScan(max_voltage_20V);	// Установить диапазон 20 В
		leso4SetSamplesNum(8192);	// Установить количество отсчетов с прибора 8192 
		leso4ReadFIFO();			// Прочитать 8192 отсчета с устройства  
		printf("Коэффициент калибровки нуля - диапазона 20В - %d\n", 
			leso4ZeroCalibration(CHANNEL_A, max_voltage_20V));	// Вычислить калибровочный коэффициент 
*/
char LESO_DLL leso4ZeroCalibration(int channel, max_voltage div);

/*!
	Установить калибровочный коэффициент амплитуды для канала на заданном диапазоне
	\param[in] channel канал который калибруется
	\param[in] div диапазон на котором происходит калибровка
	\param[in] amp калибровочный коэффициент амплитуды 
*/
void LESO_DLL leso4SetCalibrationAmp(int channel, max_voltage div, double amp);

/*!
	Получить текущий калибровочный коэффициент амплитуды для канала на текущем диапазоне 
	\param[in] channel канал который калибруется
	\param[in] div диапазон на котором происходит калибровка
	\return калибровочный коэффициент амплитуды 
*/
double LESO_DLL leso4GetCalibrationAmp(int channel, max_voltage div);

/*!
	Установить калибровочный коэффициент нуля для канала на заданном диапазоне
	\param[in] channel канал который калибруется
	\param[in] div диапазон на котором происходит калибровка
	\param[in] zero калибровочный коэффициент нуля  
*/
void LESO_DLL leso4SetCalibrationZero(int channel, max_voltage div, char zero);

/*!
	Получить текущий калибровочный коэффициент нуля для канала на текущем диапазоне 
	\param[in] channel канал который калибруется
	\param[in] div диапазон на котором происходит калибровка
	\return калибровочный коэффициент нуля  
*/
char LESO_DLL leso4GetCalibrationZero(int channel, max_voltage div);

#endif
