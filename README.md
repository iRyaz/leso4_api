# Цифровой анализатор сигналов LESO4 #
## [www.labfor.ru](http://www.labfor.ru/devices/leso4) ##

![libleso4_m.png](https://bitbucket.org/repo/KAG8zg/images/3171545814-libleso4_m.png)

C/C++ интерфейс для управление прибором.
Выполнен в виде динамической подключаемой библиотеки.
Поддерживает операционные системы: Windows, Linux

Под Windows библиотека собиралась в среде MinGW 4.9.1
Примеры программ собирались в версии Qt 5.2.0

# Сборка библиотеки для Linux #

Клонируем репозиторий с самой последней версией
```
#!

git clone https://iRyaz@bitbucket.org/iRyaz/leso4_api.git
```
Переходим в каталог и выполняем make. 


```
#!

cd ./leso4_api
make
```
Сборка библиотеки под Windows производится с помощью пакета MinGW. 
Вместо make библиотека собирается командой mingw32-make

```
#!

mingw32-make
```
 
При успешной компиляции в этой директории появится файл динамической библиотеки libLESO4.so если сборка производилась в Linux. Для Windows должен получится файл libLESO4.dll
Для работы библиотеки потребуется D2XX драйвер
Его можно скачать в архиве по адресу [http://www.ftdichip.com/Drivers/D2XX.htm](http://www.ftdichip.com/Drivers/D2XX.htm). В архиве нас интересует двоичный файл libftd2xx.so.1.3.6 (последние цифры могут меняться в зависимости от версии драйвера). Он копируется в директорию /usr/local/lib. Скачать нужно драйвер, который соответствует архитектуре компьютера, в противном случае он не будет работать. 

Для автоматической установки библиотеки в директории находится скрипт setup.sh который копирует файлы и устанавливает разрешения
```
#!

./setup.sh

```
Перед запуском скрипта в директорию с исходными кодами необходимо скопировать скаченные архивы с D2XX библиотекой для 32-х битной libftd2xx-i386-1.3.6.tgz или 64-х битной системы libftd2xx-x86_64-1.3.6.tgz.
Архив с уже скомпилированной библиотекой можно скачать [https://bitbucket.org/iRyaz/leso4_api/downloads](https://bitbucket.org/iRyaz/leso4_api/downloads)

# Пример работы с библиотекой #


```
#!c++

// file test.c
#include <stdio.h>
#include <leso4.h>

#define DEVICE_NAME "LESO4.1_ER" // Дескриптор устройства

int main()
{
	if(leso4Open(DEVICE_NAME) == LESO4_OK)
	{
		leso4EnableChannel(CHANNEL_A); // Включить канал A
		printf("Device %s open\n", DEVICE_NAME);
                printf("Version: %d\n", leso4GetVersion());
		printf("Read %d bytes\n", leso4ReadFIFO());
	}
	else
	{
		if(leso4GetLastErrorCode() == ERROR_CODE_FTDI_LIB_NOT_LOADED)
			printf("D2XX not install\n"); // Драйвер не установлен
		else
			printf("Error open device: %s\n", DEVICE_NAME); 
	}
	return 0;
}
```

Компилируем и запускаем программу (файл leso4.h для удобства заранее скопирован в /usr/include)

```
#!

gcc -o test test.c -lLESO4 -ldl
sudo ./test
```
Если заголовочный файл leso4.h находится не в /usr/include/ , то указываем нужную ключом -I


```
#!

gcc -o test test.c -lLESO4 -ldl -I./{Директория в которой находится leso.h}
```