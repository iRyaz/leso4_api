#-------------------------------------------------
#
# Project created by QtCreator 2015-06-02T10:02:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VoltmeterWidget
TEMPLATE = app

SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui

INCLUDEPATH += ../../

win32:LIBS = ../../libLESO4.dll
linux:LIBS = ../../libLESO4.so

RESOURCES += \
    def_resource.qrc

RC_FILE += device_icon.rc
