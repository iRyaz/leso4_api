<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>Widget</name>
    <message>
        <location filename="widget.ui" line="32"/>
        <source>Device Test</source>
        <translation>Тест устройства</translation>
    </message>
    <message>
        <location filename="widget.ui" line="44"/>
        <source>Channel A: </source>
        <translation>Канал А</translation>
    </message>
    <message>
        <location filename="widget.ui" line="51"/>
        <location filename="widget.ui" line="82"/>
        <location filename="widget.ui" line="113"/>
        <location filename="widget.ui" line="144"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="widget.ui" line="75"/>
        <source>Channel B: </source>
        <translation>Канал B</translation>
    </message>
    <message>
        <location filename="widget.ui" line="106"/>
        <source>Channel C: </source>
        <translation>Канал С</translation>
    </message>
    <message>
        <location filename="widget.ui" line="137"/>
        <source>Channel D: </source>
        <translation>Канал D</translation>
    </message>
    <message>
        <location filename="widget.ui" line="166"/>
        <source>Get Samples</source>
        <translation>Получить данные</translation>
    </message>
    <message>
        <location filename="widget.ui" line="186"/>
        <location filename="widget.cpp" line="47"/>
        <source>Open Device</source>
        <translation>Открыть устройство</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="29"/>
        <source>Close Device</source>
        <translation>Закрыть устройство</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="40"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="40"/>
        <source>Error Open device</source>
        <translation>Не удалось открыть устройство!! </translation>
    </message>
</context>
</TS>
