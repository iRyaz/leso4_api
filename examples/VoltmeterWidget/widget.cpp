#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    isOpen = false;

    ui->setupUi(this);
    ui->getSamplesButton->setEnabled(false);

    ui->ChannelAIndicator->setText("<font size = 3 color=\"orange\">" +
        QString::number(0) + "</font>");

    ui->ChannelBIndicator->setText("<font size = 3 color=\"green\">" +
        QString::number(0) + "</font>");

    ui->ChannelCIndicator->setText("<font size = 3 color=\"blue\">" +
        QString::number(0) + "</font>");

    ui->ChannelDIndicator->setText("<font size = 3 color=\"red\">" +
        QString::number(0) + "</font>");

    connect(ui->openDeviceButton, SIGNAL(clicked()), this, SLOT(OpenDevice()));
    connect(ui->getSamplesButton, SIGNAL(clicked()), this, SLOT(GetSamples()));
}

Widget::~Widget()
{
    delete ui;
    leso4CloseDevice();                         // Закрыть устройство
}

void Widget::OpenDevice()                       // Нажатие кнопки OpenDevice
{
    if(isOpen == false)                         // Если прибор не открыт, то попытаться открыть, и включить каналы
    {
        if(leso4Open(LESO4_DEVICE_NAME) != -1)        // Открыть прибор
        {
            ui->getSamplesButton->setEnabled(true);
            ui->openDeviceButton->setText(tr("Close Device"));
            isOpen = true;
            leso4EnableChannel(CHANNEL_A);
            leso4EnableChannel(CHANNEL_B);
            leso4EnableChannel(CHANNEL_C);
            leso4EnableChannel(CHANNEL_D);
        }
        else                                    // Если открыть не получилось
        {
            isOpen = false;
            ui->getSamplesButton->setEnabled(false);

            if(leso4GetLastErrorCode() == ERROR_CODE_FTDI_LIB_NOT_LOADED)
            {
                QMessageBox::critical(this, tr("Error"), tr("FTDI driver not loaded"));
                //std::exit(0);
            }
            else
                QMessageBox::critical(this, tr("Error"), tr("Error Open device"));
        }
    }
    else                                        // Если прибор уже был открыт, то выключить каналы и закрыть прибор
    {
        ui->getSamplesButton->setEnabled(false);
        leso4CloseDevice();
        ui->openDeviceButton->setText(tr("Open Device"));
        isOpen = false;
    }
}

void Widget::GetSamples()                       // Нажатие кнопки GetSample
{
    int rx = 0;
    rx = leso4ReadFIFO();    // Прочитать данные с устройства
    showSamples();                          // Вывести данные на экран
    qDebug() << "Receive bytes: " << rx;
}

void Widget::showSamples()
{
    ui->ChannelAIndicator->setText("<font size = 3 color=\"orange\">" +
        QString::number(((double*)leso4GetData(NULL, CHANNEL_A))[BYTE_NUM]) + "</font>");

    ui->ChannelBIndicator->setText("<font size = 3 color=\"green\">" +
        QString::number(((double *)leso4GetData(NULL, CHANNEL_B))[BYTE_NUM]) + "</font>");

    ui->ChannelCIndicator->setText("<font size = 3 color=\"blue\">" +
        QString::number(((double *)leso4GetData(NULL, CHANNEL_C))[BYTE_NUM]) + "</font>");

    ui->ChannelDIndicator->setText("<font size = 3 color=\"red\">" +
        QString::number(((double *)leso4GetData(NULL, CHANNEL_D))[BYTE_NUM]) + "</font>");
}
