/*
 * Пример программы для работы с библиотекой leso_api
 * Все нужные библиотечные функции находятся в файле leso.h
*/

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMessageBox>
#include <QDebug>

#include <leso4.h>

#define BYTE_NUM 65     // Номер байта в массиве данных

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

    bool isOpen;        // Флаг показывающий, открыто ли устройство

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void showSamples();

public slots:
    void OpenDevice();
    void GetSamples();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
