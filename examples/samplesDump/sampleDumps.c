#include <stdio.h>
#include <stdlib.h>
#include <leso4.h>

#define DEVICE_NAME "LESO4.1_ER"

void usage()
{
	printf(" %s - not open\n", DEVICE_NAME);
	printf("Device not connect or d2xx driver not installed\n");
}

int main() 
{
	if(leso4Open(DEVICE_NAME) == -1)
	{
		usage();
		return -1;
	}
	
	leso4EnableChannel(CHANNEL_A);
	
	int rx = leso4ReadFIFO();
	
	printf("Read bytes: %d", rx);
	
	double *samples_ptr = leso4GetData(NULL, CHANNEL_A);
	
	int i;
	for(i = 0; i < leso4GetChannelFIFOSize(); i++)
		printf("%f\n", samples_ptr[i]);
	
	printf("\n");
	
	return 0;
}
