/**
\file signalplotwidget.h
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details Пример простого виджета отображающего сигнал
\version 0.1
\date 27.05.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef SIGNALPLOTWIDGET_H
#define SIGNALPLOTWIDGET_H

#include <QWidget>
#include <QTimer>

#include <leso4.h>

#include "signaldiagram.h"

#define TIMEOUT 20

#define DEVICE_DESCRIPTOR "LESO4.1_ER"                          //Дескриптор устройства - зашит в FTDI

namespace Ui {
class SignalPlotWidget;
}

class SignalPlotWidget : public QWidget
{
    Q_OBJECT

    bool plottingStart;

    QTimer recvTimer;
    SignalDiagram signalScene;

    void setStatus(int _status);
    void setVersionLabel(int version_num);
    void updateSettings();                                      // Сопоставить настройки интерфейса с настройками прибора

    void initUI();

    void setRXLabel(int rx);                                    // Отобразить количество пойманый байт
    QString dialPos2Text(int dialPos);
    double AmpScan2Amp(int div);

    double ampA;
    double ampB;
    double ampC;
    double ampD;

public:
    explicit SignalPlotWidget(QWidget *parent = 0);
    ~SignalPlotWidget();

public slots:
    void interruptTimer();
    void plotStart();
    void plotStop();

    void changeSample(int combo_box_index);                    // Изменение кол-ва отсчетов, которые возвращает прибор
    void changeTimeScan(int dial_pos);                         // Переключить развертку по времени

    void changeAmpScanChannelA(int dial_pos);                  // Переключить развертку по времени для канала A
    void changeAmpScanChannelB(int dial_pos);                  // Переключить развертку по времени для канала B
    void changeAmpScanChannelC(int dial_pos);                  // Переключить развертку по времени для канала C
    void changeAmpScanChannelD(int dial_pos);                  // Переключить развертку по времени для канала D

    void channelAToggleSlot(bool isOn);                        // Включить или выключить канал А
    void channelBToggleSlot(bool isOn);                        // Включить или выключить канал B
    void channelCToggleSlot(bool isOn);                        // Включить или выключить канал C
    void channelDToggleSlot(bool isOn);                        // Включить или выключить канал D

    void channelAOpenModeSlot(bool open);                      // Режим закрытый/открытый вход для канала А
    void channelBOpenModeSlot(bool open);                      // Режим закрытый/открытый вход для канала B
    void channelCOpenModeSlot(bool open);                      // Режим закрытый/открытый вход для канала C
    void channelDOpenModeSlot(bool open);                      // Режим закрытый/открытый вход для канала D

private:
    Ui::SignalPlotWidget *ui;
};

#endif // SIGNALPLOTWIDGET_H
