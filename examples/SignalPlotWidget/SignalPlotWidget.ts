<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>QObject</name>
    <message>
        <location filename="signaldiagram.cpp" line="77"/>
        <location filename="signaldiagram.cpp" line="85"/>
        <source>Channel A</source>
        <translation>Канал A</translation>
    </message>
    <message>
        <location filename="signaldiagram.cpp" line="90"/>
        <source>Channel B</source>
        <translation>Канал B</translation>
    </message>
    <message>
        <location filename="signaldiagram.cpp" line="95"/>
        <source>Channel C</source>
        <translation>Канал C</translation>
    </message>
    <message>
        <location filename="signaldiagram.cpp" line="100"/>
        <source>Channel D</source>
        <translation>Канал D</translation>
    </message>
    <message>
        <location filename="signaldiagram.cpp" line="119"/>
        <source>V</source>
        <translation>В</translation>
    </message>
</context>
<context>
    <name>SignalPlotWidget</name>
    <message>
        <location filename="signalplotwidget.ui" line="26"/>
        <source>SignalPlotWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="51"/>
        <source>Time Scan</source>
        <translation>Развертка по времени</translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="57"/>
        <source>200</source>
        <translation></translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="91"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="101"/>
        <source>Stop</source>
        <translation>Стоп</translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="120"/>
        <source>Channel A</source>
        <translation>Канал A</translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="129"/>
        <location filename="signalplotwidget.ui" line="190"/>
        <location filename="signalplotwidget.ui" line="246"/>
        <location filename="signalplotwidget.ui" line="298"/>
        <source>AmpScan</source>
        <translation>Развертка по амплитуде</translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="136"/>
        <location filename="signalplotwidget.ui" line="197"/>
        <location filename="signalplotwidget.ui" line="253"/>
        <location filename="signalplotwidget.ui" line="305"/>
        <source>1:500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="162"/>
        <location filename="signalplotwidget.ui" line="214"/>
        <location filename="signalplotwidget.ui" line="270"/>
        <location filename="signalplotwidget.ui" line="328"/>
        <location filename="signalplotwidget.cpp" line="378"/>
        <location filename="signalplotwidget.cpp" line="392"/>
        <location filename="signalplotwidget.cpp" line="406"/>
        <location filename="signalplotwidget.cpp" line="420"/>
        <source>Close Input</source>
        <translation>Закрытый вход</translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="181"/>
        <source>Channel B</source>
        <translation>Канал B</translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="237"/>
        <source>Channel C</source>
        <translation>Канал C</translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="289"/>
        <source>Channel D</source>
        <translation>Канал D</translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="349"/>
        <source>Status:</source>
        <translation>Статус: </translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="369"/>
        <source>RX:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="389"/>
        <source>Version: </source>
        <translation>Версия: </translation>
    </message>
    <message>
        <location filename="signalplotwidget.ui" line="411"/>
        <source>Samples number</source>
        <translation>Количество отсчетов</translation>
    </message>
    <message>
        <location filename="signalplotwidget.cpp" line="122"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="signalplotwidget.cpp" line="169"/>
        <source>Status: Device connect</source>
        <translation>Статус: Устройство подключено</translation>
    </message>
    <message>
        <location filename="signalplotwidget.cpp" line="172"/>
        <source>Status: Device not connect</source>
        <translation>Статус: Устройство не подключено</translation>
    </message>
    <message>
        <location filename="signalplotwidget.cpp" line="373"/>
        <location filename="signalplotwidget.cpp" line="387"/>
        <location filename="signalplotwidget.cpp" line="401"/>
        <location filename="signalplotwidget.cpp" line="415"/>
        <source>Open Input</source>
        <translation>Открытый вход</translation>
    </message>
</context>
</TS>
