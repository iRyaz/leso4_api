/**
\file main.cpp
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details Пример простого виджета отображающего сигнал
\version 0.1
\date 27.05.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "signalplotwidget.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator translator;
    translator.load(":/translations/ru/SignalPlotWidget.qm");
    a.installTranslator(&translator);
    SignalPlotWidget w;
    w.show();

    return a.exec();
}
