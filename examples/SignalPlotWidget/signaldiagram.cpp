/**
\file signaldiagram.cpp
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details Пример простого виджета отображающего сигнал
\version 0.1
\date 27.05.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "signaldiagram.h"

SignalDiagram::SignalDiagram()
{
    channelAEn = false;
    channelBEn = false;
    channelCEn = false;
    channelDEn = false;

    signalScene.setBackgroundBrush(QBrush(BACKGROUND_COLOR));
    channelAPen.setBrush(QBrush(DIAGRAM_COLOR_CHANNEL_A));
    channelBPen.setBrush(QBrush(DIAGRAM_COLOR_CHANNEL_B));
    channelCPen.setBrush(QBrush(DIAGRAM_COLOR_CHANNEL_C));
    channelDPen.setBrush(QBrush(DIAGRAM_COLOR_CHANNEL_D));
    beginPoint.setX(0);
    beginPoint.setY(0);
}

SignalDiagram::~SignalDiagram()
{

}

void SignalDiagram::addDataChannel(int ch, double *samplesPtr, int size)
{
    QPen *pen;

    switch(ch)
    {
    case CHANNEL_A:
        pen = &channelAPen;
    break;
    case CHANNEL_B:
        pen = &channelBPen;
    break;
    case CHANNEL_C:
        pen = &channelCPen;
    break;
    case CHANNEL_D:
        pen = &channelDPen;
    break;
    }

    beginPoint.setX(0);
    beginPoint.setY(0);

    for(int i = X_OFFSET; i < size; i+=2)
    {
        signalScene.addLine(beginPoint.x(), beginPoint.y(), (i - X_OFFSET)*X_STEP, Y_AMP * -samplesPtr[i], *pen);
        beginPoint.setX((i - X_OFFSET)* X_STEP);
        beginPoint.setY(-samplesPtr[i] * Y_AMP);
    }

}

void SignalDiagram::addDataChannel(int ch, double *samplesPtr, int size, double amp)
{
    QPen *pen;
    QString channelString = QObject::tr("Channel A");
    double text_pos_y = -200;

    switch(ch)
    {
    case CHANNEL_A:
        pen = &channelAPen;
        text_pos_y = -200;
        channelString = QObject::tr("Channel A");
    break;
    case CHANNEL_B:
        pen = &channelBPen;
        text_pos_y = -180;
        channelString = QObject::tr("Channel B");
    break;
    case CHANNEL_C:
        pen = &channelCPen;
        text_pos_y = -160;
        channelString = QObject::tr("Channel C");
    break;
    case CHANNEL_D:
        pen = &channelDPen;
        text_pos_y = -140;
        channelString = QObject::tr("Channel D");
    break;
    }

    beginPoint.setX(0);
    beginPoint.setY(0);

    double maxAmplitude = 0;

    for(int i = X_OFFSET; i < size; i+=2)
    {
        if(samplesPtr[i] > maxAmplitude)
            maxAmplitude = samplesPtr[i];

        signalScene.addLine(beginPoint.x(), beginPoint.y(), (i - X_OFFSET)*X_STEP, amp * -samplesPtr[i], *pen);
        beginPoint.setX((i - X_OFFSET)* X_STEP);
        beginPoint.setY(-samplesPtr[i] * amp);
    }

    QGraphicsTextItem *item = signalScene.addText(channelString + ": " + QString::number(maxAmplitude) + " " + QObject::tr("V"));
    item->setPos(0,text_pos_y);
    item->setDefaultTextColor(pen->color());
}

void SignalDiagram::addDataChannels(double *samplesA, double ampA,
                                    double *samplesB, double ampB,
                                    double *samplesC, double ampC,
                                    double *samplesD, double ampD, int size)
{
    signalScene.clear();

    if(channelAEn)
        addDataChannel(CHANNEL_A, samplesA, size, ampA);
    if(channelBEn)
        addDataChannel(CHANNEL_B, samplesB, size, ampB);
    if(channelCEn)
        addDataChannel(CHANNEL_C, samplesC, size, ampC);
    if(channelDEn)
        addDataChannel(CHANNEL_D, samplesD, size, ampD);
}

void SignalDiagram::chanelEnDiagram(int ch, bool en)
{
    switch(ch)
    {
    case CHANNEL_A: channelAEn = en; break;
    case CHANNEL_B: channelBEn = en; break;
    case CHANNEL_C: channelCEn = en; break;
    case CHANNEL_D: channelDEn = en; break;
    }
}
