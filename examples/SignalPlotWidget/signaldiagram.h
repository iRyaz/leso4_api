/**
\file signaldiagram.h
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details Пример простого виджета отображающего сигнал
\version 0.1
\date 27.05.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef SIGNALDIAGRAM_H
#define SIGNALDIAGRAM_H

#include <QGraphicsScene>
#include <QBrush>
#include <QPointF>
#include <QDebug>
#include <QString>
#include <QObject>
#include <QGraphicsTextItem>

#include "leso4.h"

#define BACKGROUND_COLOR Qt::black

#define DIAGRAM_COLOR_CHANNEL_A Qt::yellow
#define DIAGRAM_COLOR_CHANNEL_B Qt::green
#define DIAGRAM_COLOR_CHANNEL_C Qt::cyan
#define DIAGRAM_COLOR_CHANNEL_D Qt::red

#define PEN_CHANNEL_WIDTH 1.0f

#define X_OFFSET 30
#define X_STEP 1.0f

#define Y_AMP 20


class SignalDiagram
{
    QGraphicsScene signalScene;
    QPointF beginPoint;

    QPen channelAPen;
    QPen channelBPen;
    QPen channelCPen;
    QPen channelDPen;

    bool channelAEn;
    bool channelBEn;
    bool channelCEn;
    bool channelDEn;

public:
    SignalDiagram();
    ~SignalDiagram();

    QGraphicsScene *getScenePtr() { return &signalScene; }

    void addDataChannel(int ch, double *samplesPtr, int size);
    void addDataChannel(int ch, double *samplesPtr, int size, double amp);

    void addDataChannels(double *samplesA, double ampA,
                         double *samplesB, double ampB,
                         double *samplesC, double ampC,
                         double *samplesD, double ampD,
                         int size);

    void chanelEnDiagram(int ch, bool en);
};

#endif // SIGNALDIAGRAM_H
