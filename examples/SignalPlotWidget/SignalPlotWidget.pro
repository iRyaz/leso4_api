#-------------------------------------------------
#
# Project created by QtCreator 2015-06-18T09:46:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SignalPlotWidget
TEMPLATE = app

RC_FILE += device_icon.rc

SOURCES += main.cpp\
        signalplotwidget.cpp \
    signaldiagram.cpp

HEADERS  += signalplotwidget.h \
    signaldiagram.h

FORMS    += signalplotwidget.ui

INCLUDEPATH += ../../

win32:LIBS = ../../libLESO4.dll

RESOURCES += \
    resource.qrc
