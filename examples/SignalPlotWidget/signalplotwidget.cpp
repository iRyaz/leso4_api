/**
\file signalplotwidget.cpp
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details Пример простого виджета отображающего сигнал
\version 0.1
\date 27.05.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "signalplotwidget.h"
#include "ui_signalplotwidget.h"

SignalPlotWidget::SignalPlotWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SignalPlotWidget)
{

    ampA = AmpScan2Amp(max_voltage_20V);
    ampB = AmpScan2Amp(max_voltage_20V);
    ampC = AmpScan2Amp(max_voltage_20V);
    ampD = AmpScan2Amp(max_voltage_20V);

    ui->setupUi(this);

    updateSettings();

    connect(&recvTimer, SIGNAL(timeout()), this, SLOT(interruptTimer()));
    connect(ui->startButton, SIGNAL(clicked()), this, SLOT(plotStart()));
    connect(ui->stopButton, SIGNAL(clicked()), this, SLOT(plotStop()));
    connect(ui->samplesComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSample(int)));
    connect(ui->timeScanDial, SIGNAL(valueChanged(int)), this, SLOT(changeTimeScan(int)));

    connect(ui->ampScanChannelADial, SIGNAL(valueChanged(int)), this, SLOT(changeAmpScanChannelA(int)));
    connect(ui->ampScanChannelBDial, SIGNAL(valueChanged(int)), this, SLOT(changeAmpScanChannelB(int)));
    connect(ui->ampScanChannelCDial, SIGNAL(valueChanged(int)), this, SLOT(changeAmpScanChannelC(int)));
    connect(ui->ampScanChannelDDial, SIGNAL(valueChanged(int)), this, SLOT(changeAmpScanChannelD(int)));

    connect(ui->channelAGroupBox, SIGNAL(clicked(bool)), this, SLOT(channelAToggleSlot(bool)));
    connect(ui->channelBGroupBox, SIGNAL(clicked(bool)), this, SLOT(channelBToggleSlot(bool)));
    connect(ui->channelCGroupBox, SIGNAL(clicked(bool)), this, SLOT(channelCToggleSlot(bool)));
    connect(ui->channelDGroupBox, SIGNAL(clicked(bool)), this, SLOT(channelDToggleSlot(bool)));

    connect(ui->inputModeButtonChannelA, SIGNAL(clicked(bool)), this, SLOT(channelAOpenModeSlot(bool)));
    connect(ui->inputModeButtonChannelB, SIGNAL(clicked(bool)), this, SLOT(channelBOpenModeSlot(bool)));
    connect(ui->inputModeButtonChannelC, SIGNAL(clicked(bool)), this, SLOT(channelCOpenModeSlot(bool)));
    connect(ui->inputModeButtonChannelD, SIGNAL(clicked(bool)), this, SLOT(channelDOpenModeSlot(bool)));

    setStatus(LESO4_ERROR);

    if(leso4Open(DEVICE_DESCRIPTOR) == LESO4_OK)
    {
        setStatus(LESO4_OK);
        setVersionLabel(leso4GetVersion());
    }

    ui->signalView->setScene(signalScene.getScenePtr());
    initUI();

    leso4EnableChannel(CHANNEL_A);
    leso4EnableChannel(CHANNEL_B);
    leso4EnableChannel(CHANNEL_C);
    leso4EnableChannel(CHANNEL_D);

    plotStart();
}

SignalPlotWidget::~SignalPlotWidget()
{
    //leso4CloseDevice();
    delete ui;
}

void SignalPlotWidget::initUI()
{
    ui->samplesComboBox->addItem("1k");
    ui->samplesComboBox->addItem("2k");
    ui->samplesComboBox->addItem("4k");
    ui->samplesComboBox->addItem("8k");

    setRXLabel(0);

    signalScene.chanelEnDiagram(CHANNEL_A, true);
    signalScene.chanelEnDiagram(CHANNEL_B, true);
    signalScene.chanelEnDiagram(CHANNEL_C, true);
    signalScene.chanelEnDiagram(CHANNEL_D, true);
}

void SignalPlotWidget::updateSettings()
{
    this->setEnabled(true);

    changeSample(0);

    leso4EnableChannel(CHANNEL_A);
    leso4EnableChannel(CHANNEL_B);
    leso4EnableChannel(CHANNEL_C);
    leso4EnableChannel(CHANNEL_D);
    ui->ampScanChannelADial->setSliderPosition(max_voltage_20V);
    ui->ampScanChannelBDial->setSliderPosition(max_voltage_20V);
    ui->ampScanChannelCDial->setSliderPosition(max_voltage_20V);
    ui->ampScanChannelDDial->setSliderPosition(max_voltage_20V);
    ui->timeScanDial->setSliderPosition(6);
    //leso4TimeScan(200);

    changeAmpScanChannelA(max_voltage_20V);
    changeAmpScanChannelB(max_voltage_20V);
    changeAmpScanChannelC(max_voltage_20V);
    changeAmpScanChannelD(max_voltage_20V);
}

void SignalPlotWidget::setVersionLabel(int version_num)
{
    ui->versionLabel->setText("<font size = 5 color=\"purple\">" + tr("Version") + ": " +
                     QString::number(version_num) + "</font>");
}

void SignalPlotWidget::interruptTimer()
{
    if(leso4IsOpen())
    {
        setStatus(LESO4_OK);
        //updateSettings();
        int rx = leso4ReadFIFO();
        if(rx > 0)
        {
            signalScene.addDataChannels(((double *)leso4GetData(NULL, CHANNEL_A)), ampA,
                                        ((double *)leso4GetData(NULL, CHANNEL_B)), ampB,
                                        ((double *)leso4GetData(NULL, CHANNEL_C)), ampC,
                                        ((double *)leso4GetData(NULL, CHANNEL_D)), ampD,
                                        leso4GetChannelFIFOSize());
            setRXLabel(rx);
        }
    }
    else
    {
        if(leso4Open(DEVICE_DESCRIPTOR) == LESO4_OK)
        {
            setStatus(LESO4_OK);
            setVersionLabel(leso4GetVersion());
            updateSettings();
        }
        else
        {
            this->setEnabled(false);
            setStatus(LESO4_ERROR);
            setRXLabel(0);
            setVersionLabel(0);
        }
    }
}

void SignalPlotWidget::setStatus(int _status)
{
    QString statusLabel;

    switch(_status)
    {
    case LESO4_OK:
        statusLabel = "<font size = 5 color=\"green\">" + tr("Status: Device connect") + "</font>";
        break;
    case LESO4_ERROR:
        statusLabel = "<font size = 5 color=\"red\">" + tr("Status: Device not connect") + "</font>";
        break;
    }

    ui->deviceStatusLabel->setText(statusLabel);
}

void SignalPlotWidget::plotStart()
{
    recvTimer.start(TIMEOUT);
    plottingStart = true;
    ui->stopButton->setEnabled(true);
    ui->startButton->setEnabled(false);
}

void SignalPlotWidget::plotStop()
{
    recvTimer.stop();
    plottingStart = false;
    ui->startButton->setEnabled(true);
    ui->stopButton->setEnabled(false);
}

void SignalPlotWidget::changeSample(int combo_box_index)
{
    int samples_num = 0;

    switch(combo_box_index)
    {
    case 3: samples_num = 8192; break;
    case 2: samples_num = 4096; break;
    case 1: samples_num = 2048; break;
    case 0: samples_num = 1024; break;
    default: samples_num = 1024;
    }

    leso4SetSamplesNum(samples_num);
}

void SignalPlotWidget::setRXLabel(int rx)
{
    QString statusRXLabel;
    if(rx != 0)
        statusRXLabel = "<font size = 5 color=\"green\">RX: " + QString::number(rx) + "</font>";
    else
        statusRXLabel = "<font size = 5 color=\"red\">RX: " + QString::number(rx) + "</font>";

    ui->rxLabel->setText(statusRXLabel);
}

void SignalPlotWidget::changeTimeScan(int dial_pos)
{
    sample_frequency sampleFreq = sampe_frequency_25kHz;

    switch(dial_pos)
    {
    case 0: sampleFreq = sampe_frequency_25kHz; break;
    case 1: sampleFreq = sampe_frequency_50kHz; break;
    case 2: sampleFreq = sampe_frequency_100kHz; break;
    case 3: sampleFreq = sampe_frequency_250kHz; break;
    case 4: sampleFreq = sampe_frequency_500kHz; break;
    case 5: sampleFreq = sampe_frequency_1000kHz; break;
    case 6: sampleFreq = sampe_frequency_2500kHz; break;
    case 7: sampleFreq = sampe_frequency_5MHz; break;
    case 8: sampleFreq = sampe_frequency_10MHz; break;
    case 9: sampleFreq = sampe_frequency_25MHz; break;
    case 10: sampleFreq = sampe_frequency_50MHz; break;
    }

    ui->timeScanLabel->setText(QString::number((int)sampleFreq));
    leso4SetSamplFreq(sampleFreq);
}

double SignalPlotWidget::AmpScan2Amp(int div)
{
    double ret;
    switch(div)
    {
    case max_voltage_20V: ret = 20; break;
    case max_voltage_8V: ret = 40; break;
    case max_voltage_4V: ret = 80; break;
    case max_voltage_2V: ret = 160; break;
    case max_voltage_800mV: ret = 320; break;
    case max_voltage_400mV: ret = 640; break;
    case max_voltage_200mV: ret = 1280; break;
    case max_voltage_80mV: ret = 2560; break;
    case max_voltage_40mV: ret = 5120; break;
    }

    return ret;
}

void SignalPlotWidget::changeAmpScanChannelA(int dial_pos)
{
    max_voltage vDiv = max_voltage_20V;
    switch(dial_pos)
    {
    case 0: vDiv = max_voltage_20V; break;
    case 1: vDiv = max_voltage_8V; break;
    case 2: vDiv = max_voltage_4V; break;
    case 3: vDiv = max_voltage_2V; break;
    case 4: vDiv = max_voltage_800mV; break;
    case 5: vDiv = max_voltage_400mV; break;
    case 6: vDiv = max_voltage_200mV; break;
    case 7: vDiv = max_voltage_80mV; break;
    case 8: vDiv = max_voltage_40mV; break;
    }

    leso4SetAmplitudeScan(CHANNEL_A, vDiv);
    ui->divALabel->setText("<font size = 5 color=\"orange\">" + dialPos2Text(dial_pos) + "</font>");
    ampA = AmpScan2Amp(dial_pos);
}

void SignalPlotWidget::changeAmpScanChannelB(int dial_pos)
{
    max_voltage vDiv = max_voltage_20V;
    switch(dial_pos)
    {
    case 0: vDiv = max_voltage_20V; break;
    case 1: vDiv = max_voltage_8V; break;
    case 2: vDiv = max_voltage_4V; break;
    case 3: vDiv = max_voltage_2V; break;
    case 4: vDiv = max_voltage_800mV; break;
    case 5: vDiv = max_voltage_400mV; break;
    case 6: vDiv = max_voltage_200mV; break;
    case 7: vDiv = max_voltage_80mV; break;
    case 8: vDiv = max_voltage_40mV; break;
    }

    leso4SetAmplitudeScan(CHANNEL_B, vDiv);
    ui->divBLabel->setText("<font size = 5 color=\"green\">" + dialPos2Text(dial_pos) + "</font>");
    ampB = AmpScan2Amp(dial_pos);
}

void SignalPlotWidget::changeAmpScanChannelC(int dial_pos)
{
    max_voltage vDiv = max_voltage_20V;
    switch(dial_pos)
    {
    case 0: vDiv = max_voltage_20V; break;
    case 1: vDiv = max_voltage_8V; break;
    case 2: vDiv = max_voltage_4V; break;
    case 3: vDiv = max_voltage_2V; break;
    case 4: vDiv = max_voltage_800mV; break;
    case 5: vDiv = max_voltage_400mV; break;
    case 6: vDiv = max_voltage_200mV; break;
    case 7: vDiv = max_voltage_80mV; break;
    case 8: vDiv = max_voltage_40mV; break;
    }

    leso4SetAmplitudeScan(CHANNEL_C, vDiv);
    ui->divCLabel->setText("<font size = 5 color=\"cyan\">" + dialPos2Text(dial_pos) + "</font>");
    ampC = AmpScan2Amp(dial_pos);
}

void SignalPlotWidget::changeAmpScanChannelD(int dial_pos)
{
    max_voltage vDiv = max_voltage_20V;
    switch(dial_pos)
    {
    case 0: vDiv = max_voltage_20V; break;
    case 1: vDiv = max_voltage_8V; break;
    case 2: vDiv = max_voltage_4V; break;
    case 3: vDiv = max_voltage_2V; break;
    case 4: vDiv = max_voltage_800mV; break;
    case 5: vDiv = max_voltage_400mV; break;
    case 6: vDiv = max_voltage_200mV; break;
    case 7: vDiv = max_voltage_80mV; break;
    case 8: vDiv = max_voltage_40mV; break;
    }

    leso4SetAmplitudeScan(CHANNEL_D, vDiv);
    ui->divDLabel->setText("<font size = 5 color=\"red\">" + dialPos2Text(dial_pos) + "</font>");
    ampD = AmpScan2Amp(dial_pos);
}

QString SignalPlotWidget::dialPos2Text(int dialPos)
{
    QString divLabel = "1:500";

    switch(dialPos)
    {
    case max_voltage_20V: divLabel = "1:500"; break;
    case max_voltage_8V: divLabel = "1:200"; break;
    case max_voltage_4V: divLabel = "1:100"; break;
    case max_voltage_2V: divLabel = "1:50"; break;
    case max_voltage_800mV: divLabel = "1:20"; break;
    case max_voltage_400mV: divLabel = "1:10"; break;
    case max_voltage_200mV: divLabel = "1:5"; break;
    case max_voltage_80mV: divLabel = "1:2"; break;
    case max_voltage_40mV: divLabel = "1:1"; break;
    }

    return divLabel;
}

void SignalPlotWidget::channelAToggleSlot(bool isOn)
{
    if(isOn)
    {
        leso4EnableChannel(CHANNEL_A);
    }
    else
    {
        leso4DisableChannel(CHANNEL_A);
    }

    signalScene.chanelEnDiagram(CHANNEL_A, isOn);
}

void SignalPlotWidget::channelBToggleSlot(bool isOn)
{
    if(isOn)
    {
        leso4EnableChannel(CHANNEL_B);
    }
    else
    {
        leso4DisableChannel(CHANNEL_B);
    }

    signalScene.chanelEnDiagram(CHANNEL_B, isOn);
}

void SignalPlotWidget::channelCToggleSlot(bool isOn)
{
    if(isOn)
    {
        leso4EnableChannel(CHANNEL_C);
    }
    else
    {
        leso4DisableChannel(CHANNEL_C);
    }

    signalScene.chanelEnDiagram(CHANNEL_C, isOn);
}

void SignalPlotWidget::channelDToggleSlot(bool isOn)
{
    if(isOn)
    {
        leso4EnableChannel(CHANNEL_D);
    }
    else
    {
        leso4DisableChannel(CHANNEL_D);
    }

    signalScene.chanelEnDiagram(CHANNEL_D, isOn);
}

void SignalPlotWidget::channelAOpenModeSlot(bool open)
{
    if(open)
    {
        leso4SetInputMode(CHANNEL_A, input_mode_close);
        ui->inputModeButtonChannelA->setText(tr("Open Input"));
    }
    else
    {
        leso4SetInputMode(CHANNEL_A, input_mode_open);
        ui->inputModeButtonChannelA->setText(tr("Close Input"));
    }
}

void SignalPlotWidget::channelBOpenModeSlot(bool open)
{
    if(open)
    {
        leso4SetInputMode(CHANNEL_B, input_mode_close);
        ui->inputModeButtonChannelB->setText(tr("Open Input"));
    }
    else
    {
        leso4SetInputMode(CHANNEL_B, input_mode_open);
        ui->inputModeButtonChannelB->setText(tr("Close Input"));
    }
}

void SignalPlotWidget::channelCOpenModeSlot(bool open)
{
    if(open)
    {
        leso4SetInputMode(CHANNEL_C, input_mode_close);
        ui->inputModeButtonChannelC->setText(tr("Open Input"));
    }
    else
    {
        leso4SetInputMode(CHANNEL_C, input_mode_open);
        ui->inputModeButtonChannelC->setText(tr("Close Input"));
    }
}

void SignalPlotWidget::channelDOpenModeSlot(bool open)
{
    if(open)
    {
        leso4SetInputMode(CHANNEL_D, input_mode_close);
        ui->inputModeButtonChannelD->setText(tr("Open Input"));
    }
    else
    {
        leso4SetInputMode(CHANNEL_D, input_mode_open);
        ui->inputModeButtonChannelD->setText(tr("Close Input"));
    }
}
