#!/bin/bash


DEVICE_DESCRIPTION="LESO4.1_ER"

NAME_LIBFT="libftd2xx"
DRNAME="libftd2xx-$(uname -i)-1.3.6.tgz"
PATH_LIBFT="/usr/local/lib"

NAME_LIBLESO="libLESO4.so"
PATH_LIBLESO4="/usr/lib"

PATH_UDEV_RULES="/etc/udev/rules.d"
NAME_UDEV_RULES="99-leso4.rules"


if [[ $EUID -ne 0 ]]; then
	echo "Для запуска этого скрипта нужны привилегии root"
	exit 1
fi

case "$1" in
	install)
	# Устанавливаем драфве ftdi
		if [ -f ${PATH_LIBFT}/${NAME_LIBFT}.so ]; then
			echo "Драйвер ${NAME_LIBFT} уже установлен"
		else
		
			if [ -f ${DRNAME} ]; then
				tar xfz ${DRNAME}			#Распоковываем архив
			else
				echo "Архив с драйвером (${DRNAME}) не найден"
				exit 1
			fi
			
			cp release/build/${NAME_LIBFT}.* ${PATH_LIBFT}
			rm -fr release					#Удаляем за собой
			chmod 0755 ${PATH_LIBFT}/${NAME_LIBFT}.so.1.3.6
			ln -sf ${PATH_LIBFT}/${NAME_LIBFT}.so.1.3.6 ${PATH_LIBFT}/${NAME_LIBFT}.so
			echo "Драйвер ${NAME_LIBFT} установлен"
		fi
		
	# Устанавливаем диблиотеку LESO4		
		if [ -f ${PATH_LIBLESO4}/${NAME_LIBLESO} ]; then
			echo "Библиотека ${NAME_LIBLESO} уже установлена"
		else
		
			if [ -f ${NAME_LIBLESO} ]; then
				tar xfz ${DRNAME}			#Распоковываем архив
			else
				echo "Файл ${NAME_LIBLESO} не найден"
				exit 1
			fi
			
			cp ${NAME_LIBLESO} ${PATH_LIBLESO4}
			chmod 0755 ${PATH_LIBLESO4}/${NAME_LIBLESO}
			echo "Библиотека ${NAME_LIBLESO} установлена"
		fi		
	# Создаем правило udev
	echo "ATTRS{idVendor}==\"0403\", ATTRS{idProduct}==\"6014\", MODE=\"0666\"" > ${PATH_UDEV_RULES}/${NAME_UDEV_RULES}
	echo "ATTRS{idVendor}==\"0403\", ATTRS{idProduct}==\"6014\", ATTRS{product}==\"${DEVICE_DESCRIPTION}\", RUN+=\"/bin/sh -c 'echo \$kernel > /sys/bus/usb/drivers/ftdi_sio/unbind'\" " >> ${PATH_UDEV_RULES}/${NAME_UDEV_RULES}
	chmod 0644 ${PATH_UDEV_RULES}/${NAME_UDEV_RULES}
	echo "Создано правило ${PATH_UDEV_RULES}/${NAME_UDEV_RULES} для LESO4"
	
	echo "" >> ${PATH_LIBFT}/${NAME_UDEV_RULES}
		
		;;
	remove)
		
		while true; do
			read -p "Удалить драйвер ${NAME_LIBFT}? (y/n) " yn
			case $yn in
				[Yy]* ) 
					rm -fr ${PATH_LIBFT}/${NAME_LIBFT}.*
					echo "Драйвер ${NAME_LIBFT} удален"
					break
					;;
				[Nn]* ) 
					echo "Драйвер ${NAME_LIBFT} оставлен в системе"
					break
					;;
				* ) 
				echo "Пожалуйста, ответте yes или no"
				;;
			esac
		done
		
		while true; do
			read -p "Удалить библиотеку ${NAME_LIBLESO}? (y/n) " yn
			case $yn in
				[Yy]* ) 
					rm -fr ${PATH_LIBLESO4}/${NAME_LIBLESO}
					echo "Библиотека ${NAME_LIBLESO} удалена"
					break
					;;
				[Nn]* ) 
					echo "Библиотека ${NAME_LIBLESO} оставлена в системе"
					break
					;;
				* ) 
				echo "Пожалуйста, ответте yes или no"
				;;
			esac
		done
		
		while true; do
			read -p "Удалить правило udev для LESO4? (y/n) " yn
			case $yn in
				[Yy]* ) 
					rm -fr ${PATH_UDEV_RULES}/${NAME_UDEV_RULES}
					echo "Правило ${NAME_UDEV_RULES} удалено"
					break
					;;
				[Nn]* ) 
					echo "Правило ${NAME_UDEV_RULES} оставлено в системе"
					break
					;;
				* ) 
				echo "Пожалуйста, ответте yes или no"
				;;
			esac
		done

		;;

	*)
		echo "Usage: $0 {install|remove}"
		exit 1
		;;
esac


exit 0
